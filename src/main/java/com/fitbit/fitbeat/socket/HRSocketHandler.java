package com.fitbit.fitbeat.socket;

import com.fitbit.fitbeat.config.MusicStorageConfig;
import com.fitbit.fitbeat.db.entities.User;
import com.fitbit.fitbeat.db.entities.UserSongHistory;
import com.fitbit.fitbeat.db.repositories.UserSongHistoryRepository;
import com.fitbit.fitbeat.service.HRService;
import com.fitbit.fitbeat.service.UserService;
import com.fitbit.fitbeat.util.BpmUtil;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author Anton Kholodok
 */
@Component
public class HRSocketHandler extends TextWebSocketHandler {

    private static final Gson gson = new Gson();

    private UserService userService;
    private HRService hrService;
    private UserSongHistoryRepository userSongHistoryRepository;
    private MusicStorageConfig musicStorageConfig;
    private OAuth2RestTemplate fitbitRestTemplate;

    private Map<String, WebSocketSession> sessions = new ConcurrentHashMap<>();

    @Autowired
    public HRSocketHandler(UserService userService,
                           HRService hrService,
                           UserSongHistoryRepository userSongHistoryRepository,
                           MusicStorageConfig musicStorageConfig,
                           @Qualifier("fitbitRestTemplate") OAuth2RestTemplate fitbitRestTemplate) {
        this.userService = userService;
        this.hrService = hrService;
        this.userSongHistoryRepository = userSongHistoryRepository;
        this.musicStorageConfig = musicStorageConfig;
        this.fitbitRestTemplate = fitbitRestTemplate;
    }

    @Override
    public void afterConnectionEstablished(WebSocketSession session) throws Exception {
        System.out.println("HR Socket connection established: " + session.getId());
        this.sessions.putIfAbsent(session.getId(), session);
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
        super.afterConnectionClosed(session, status);
        this.sessions.remove(session.getId());
        System.out.println("HR Socket connection closed");
    }

    @Override
    protected void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {
        System.out.println("HR Socket text message");
        super.handleTextMessage(session, message);
    }

    private class HRResponse {
        private int value;
        private boolean nextSong;

        public int getValue() {
            return value;
        }

        public void setValue(int value) {
            this.value = value;
        }

        public boolean isNextSong() {
            return nextSong;
        }

        public void setNextSong(boolean nextSong) {
            this.nextSong = nextSong;
        }
    }

    @Scheduled(fixedDelay = 5000)
    public void sendHr() {
        sessions.values().stream().forEach(this::handleSession);
    }

    private void handleSession(WebSocketSession session) {
        if (session != null && session.isOpen()) {

            SecurityContext securityContext = (SecurityContext) session.getAttributes().get("SPRING_SECURITY_CONTEXT");
            SecurityContextHolder.getContext().setAuthentication(securityContext.getAuthentication());

            User user = userService.getOrCreateUser();
            UserSongHistory userSongHistory = userSongHistoryRepository.findFirstByUserIdOrderByCreatedDesc(user.getId());

            OAuth2RestTemplate template = new OAuth2RestTemplate(
                    fitbitRestTemplate.getResource(),
                    (OAuth2ClientContext) session.getAttributes().get("scopedTarget.oauth2ClientContext"));
            int rate = hrService.getUserHR(user, template);
            boolean shouldChangeSong = userSongHistory == null
                    ? true
                    : BpmUtil.normalizeBpm(rate, musicStorageConfig.isRoundingUp()) != userSongHistory.getBpm();

            try {
                HRResponse hrResponse = new HRResponse();
                hrResponse.setValue(rate);
                hrResponse.setNextSong(shouldChangeSong);
                session.sendMessage(new TextMessage(gson.toJson(hrResponse)));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
