package com.fitbit.fitbeat.util;

import java.nio.file.Path;

/**
 * @author Boris Skarabahaty.
 */
public class Song {
    private final int fromBpm;
    private final int toBpm;
    private final Genre genre;
    private final Path path;

    public Song(int fromBpm, int toBpm, Genre genre, Path path) {
        this.fromBpm = fromBpm;
        this.toBpm = toBpm;
        this.genre = genre;
        this.path = path;
    }

    public int getFromBpm() {
        return fromBpm;
    }

    public int getToBpm() {
        return toBpm;
    }

    public Genre getGenre() {
        return genre;
    }

    public Path getPath() {
        return path;
    }
}
