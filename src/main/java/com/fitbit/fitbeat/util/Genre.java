package com.fitbit.fitbeat.util;

import java.util.Arrays;

/**
 * @author Boris Skarabahaty.
 */
public enum Genre {
    ALL("All"),
    ROCK("Rock"),
    METAL("Metal"),
    ELECTRONIC("Electronic"),
    COUNTRY("Country"),
    POP("Pop");

    private final String value;

    Genre(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static Genre fromValue(String value) {
        return Arrays.stream(values())
            .filter(v -> v.value.equalsIgnoreCase(value))
            .findFirst()
            .orElseThrow(IllegalArgumentException::new);
    }
}
