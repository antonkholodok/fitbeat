package com.fitbit.fitbeat.util;

import com.fitbit.fitbeat.rest.dto.StreamableSong;

import org.apache.commons.io.filefilter.WildcardFileFilter;

import java.io.File;
import java.io.FileFilter;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @author Boris Skarabahaty.
 */
public abstract class SongUtil {
    private SongUtil() {
    }

    public static Path findSongByName(String musicStorage, String song) {
        return Paths.get(musicStorage, song);
    }

    public static List<StreamableSong> findSongsByBPM(String musicStorage, int bpm, Collection<Genre> genres) {
        if (genres.isEmpty()) {
            return Collections.emptyList();
        }

        List<StreamableSong> songs = new ArrayList<>();
        Set<Genre> unique = new HashSet<>();
        for (Genre genre : genres) {
            if (genre == Genre.ALL) {
                unique.addAll(Arrays.asList(Genre.values()));
                continue;
            }

            unique.add(genre);
        }

        for (Genre genre : unique) {
            if (genre == Genre.ALL) {
                continue;
            }

            String wildcard = buildWildcard(bpm, genre);
            FileFilter filter = new WildcardFileFilter(wildcard);
            File storage = new File(musicStorage);
            File[] files = storage.listFiles(filter);
            songs.addAll(files == null
                ? Collections.emptyList()
                : Arrays.stream(files)
                .map(f -> new StreamableSong(getPath(f.getName()), getSongName(f.getName()), bpm))
                .collect(Collectors.toList()));
        }

        if (!songs.isEmpty()) {
            return songs.stream().sorted(Comparator.comparing(StreamableSong::getName)).collect(Collectors.toList());
        }

        int nextBpm = BpmUtil.nextBpm(bpm);
        if (nextBpm == bpm) {
            return songs;
        }

        return findSongsByBPM(musicStorage, nextBpm, genres);
    }

    private static String getSongName(String filename) {
        String withExtension = filename.split("_")[2];
        return withExtension.substring(0, withExtension.indexOf("."));
    }

    private static String getPath(String filename) {
        return filename.substring(0, filename.indexOf("."));
    }

    private static String buildWildcard(int bpm, Genre genre) {
        return genre == Genre.ALL
            ? String.format("%d_*_*", bpm)
            : String.format("%d_%s_*", bpm, genre.getValue().toLowerCase());
    }
}
