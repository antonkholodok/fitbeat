package com.fitbit.fitbeat.util;

/**
 * @author Boris Skarabahaty.
 */
public abstract class BpmUtil {
    private static final int bpmMin = 50;
    private static final int bpmMax = 180;
    private static final int bpmInterval = 5;

    private BpmUtil() {
    }

    public static int bpmFromSpeed(int speed, double strideLength, boolean roundingUp) {
        int bpm = Math.max(bpmMin, Math.min(bpmMax, (int) ((speed * 1000 / strideLength) / 60)));
        return normalizeBpm(bpm, roundingUp);
    }

    public static int normalizeBpm(int bpm, boolean roundingUp) {
        int reminder = bpm % bpmInterval;
        if (reminder == 0) {
            return bpm;
        }

        return roundingUp ? bpm + bpmInterval - reminder : bpm - reminder;
    }

    public static int nextBpm(int normalizedBpm) {
        return Math.min(bpmMax, normalizedBpm + bpmInterval);
    }
}
