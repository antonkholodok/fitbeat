package com.fitbit.fitbeat.service;

import com.fitbit.fitbeat.db.entities.User;
import com.fitbit.fitbeat.db.entities.UserHeartRate;
import com.fitbit.fitbeat.db.repositories.UserHeartRateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.stereotype.Service;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.concurrent.TimeUnit;

/**
 * @author Anton Kholodok
 */
@Service
public class HRService {

    private static final String HR_URL = "https://api.fitbit.com/1/user/%s/activities/heart/date/today/1d/1min/time/%s/%s.json";

    private UserHeartRateRepository userHeartRateRepository;

    @Autowired
    public HRService(UserHeartRateRepository userHeartRateRepository) {
        this.userHeartRateRepository = userHeartRateRepository;
    }

    public int getUserHR(User user,
                         OAuth2RestTemplate fitbitRestTemplate) {

        UserHeartRate userHeartRate = userHeartRateRepository.findByUserId(user.getId());

        Date currentDate = new Date();
        if (!userHeartRate.getExpires().after(currentDate)) {
            SimpleDateFormat dateFormat = new SimpleDateFormat("HH:mm");
            String url = String.format(
                    HR_URL,
                    user.getFitbitId(),
                    dateFormat.format(userHeartRate.getUpdated()),
                    dateFormat.format(currentDate));
            ResponseEntity<Object> response = fitbitRestTemplate.getForEntity(url, Object.class);
            System.out.println(response);

            Map<String, Object> body = (Map<String, Object>) response.getBody();
            Map<String, Object> intradayData = (Map<String, Object>) body.get("activities-heart-intraday");
            List<Map<String, Object>> dataset = (List<Map<String, Object>>) intradayData.get("dataset");

            if (dataset != null && dataset.size() > 0) {
                Map<String, Object> latestData = dataset.get(dataset.size() - 1);
                Integer value = (Integer) latestData.get("value");
                userHeartRate.setRate(value);
                userHeartRate.setUpdated(currentDate);
                userHeartRate.setExpires(new Date(currentDate.getTime() + TimeUnit.SECONDS.toMillis(30)));
                userHeartRateRepository.save(userHeartRate);
            }
        }

        return Optional.ofNullable(userHeartRate)
                .map(r -> r.getRate())
                .orElse(60);
    }
}
