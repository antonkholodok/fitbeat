package com.fitbit.fitbeat.service;

import com.fitbit.fitbeat.db.entities.User;
import com.fitbit.fitbeat.db.entities.UserHeartRate;
import com.fitbit.fitbeat.db.repositories.UserHeartRateRepository;
import com.fitbit.fitbeat.db.repositories.UserRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Map;

/**
 * @author Anton Kholodok
 */
@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private UserHeartRateRepository userHeartRateRepository;

    public User getOrCreateUser() {
        OAuth2Authentication auth = (OAuth2Authentication) SecurityContextHolder.getContext().getAuthentication();
        Map<String, Object> userData = (Map<String, Object>) auth.getUserAuthentication().getPrincipal();
        String fitbitId = (String) userData.get("encodedId");
        User user = userRepository.findByFitbitId(fitbitId);
        if (user == null) {
            createUser(
                (String) userData.get("fullName"),
                fitbitId,
                ((Number) userData.get("strideLengthRunning")).doubleValue(),
                ((Number) userData.get("strideLengthWalking")).doubleValue());
        }
        return user;
    }

    public User createUser(String name,
                           String fitbitId,
                           Double strideLengthRunning,
                           Double strideLengthWalking) {
        User user = new User(fitbitId, name, strideLengthRunning, strideLengthWalking);
        userRepository.save(user);
        UserHeartRate userHeartRate = new UserHeartRate(user.getId(), new Date(), new Date(), 60);
        userHeartRateRepository.save(userHeartRate);
        return user;
    }
}
