package com.fitbit.fitbeat.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author Boris Skarabahaty.
 */
@ConfigurationProperties("music.storage")
public class MusicStorageConfig {
    private String path;
    private boolean roundingUp;

    public boolean isRoundingUp() {
        return roundingUp;
    }

    public void setRoundingUp(boolean roundingUp) {
        this.roundingUp = roundingUp;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }
}
