package com.fitbit.fitbeat.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.security.oauth2.resource.UserInfoTokenServices;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.oauth2.client.OAuth2RestTemplate;
import org.springframework.security.oauth2.client.filter.OAuth2ClientAuthenticationProcessingFilter;
import org.springframework.security.oauth2.client.resource.OAuth2ProtectedResourceDetails;
import org.springframework.security.oauth2.client.token.grant.code.AuthorizationCodeResourceDetails;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableOAuth2Client;

import javax.servlet.Filter;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Anton Kholodok
 */
@Configuration
@EnableOAuth2Client
@ConfigurationProperties(prefix = "oauth2.fitbit.client")
public class FitbitOAuth2Config {

    private String clientId;
    private String clientSecret;
    private String accessTokenUri;
    private String userAuthorizationUri;
    private String userInfoEndpointUrl;
    private List<String> scope = new ArrayList<>();

    @Autowired
    private OAuth2ClientContext oAuth2ClientContext;


    @Bean
    public OAuth2RestTemplate fitbitRestTemplate() {
        return new OAuth2RestTemplate(fitbitResourceDetails(), oAuth2ClientContext);
    }

    @Bean
    public Filter fitbitOAuth2Filter() {
        OAuth2ClientAuthenticationProcessingFilter filter = new OAuth2ClientAuthenticationProcessingFilter("/login/fitbit");
        filter.setTokenServices(new UserInfoTokenServices(userInfoEndpointUrl, clientId));
        filter.setRestTemplate(fitbitRestTemplate());
        return filter;
    }

    private OAuth2ProtectedResourceDetails fitbitResourceDetails() {
        AuthorizationCodeResourceDetails resource = new AuthorizationCodeResourceDetails();

        resource.setClientId(clientId);
        resource.setClientSecret(clientSecret);
        resource.setAccessTokenUri(accessTokenUri);
        resource.setUserAuthorizationUri(userAuthorizationUri);
        resource.setScope(scope);

        //resource.setUseCurrentUri(false);

        return resource;
    }


    public List<String> getScope() {
        return scope;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }

    public void setAccessTokenUri(String accessTokenUri) {
        this.accessTokenUri = accessTokenUri;
    }

    public void setUserAuthorizationUri(String userAuthorizationUri) {
        this.userAuthorizationUri = userAuthorizationUri;
    }

    public void setUserInfoEndpointUrl(String userInfoEndpointUrl) {
        this.userInfoEndpointUrl = userInfoEndpointUrl;
    }
}
