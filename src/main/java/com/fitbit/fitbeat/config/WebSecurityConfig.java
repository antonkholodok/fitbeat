package com.fitbit.fitbeat.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.web.authentication.LoginUrlAuthenticationEntryPoint;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.Filter;

/**
 * @author Anton Kholodok
 */
@Configuration
@Import(FitbitOAuth2Config.class)
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    @Qualifier("fitbitOAuth2Filter")
    private Filter fitbitOAuth2Filter;

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            .antMatcher("/**")
            .csrf()
            .disable()
            .authorizeRequests()
            .antMatchers("/index", "/login/fitbit").permitAll()
            .anyRequest().authenticated()
            .and()
            .exceptionHandling().authenticationEntryPoint(new LoginUrlAuthenticationEntryPoint("/login/fitbit"))
            .and()
            .logout().logoutUrl("/logout").logoutSuccessUrl("/index").permitAll()
            .and()
            .addFilterBefore(fitbitOAuth2Filter, BasicAuthenticationFilter.class);
    }
}
