package com.fitbit.fitbeat;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class FitbeatApplication {

    public static void main(String[] args) {
        SpringApplication.run(FitbeatApplication.class, args);
    }
}
