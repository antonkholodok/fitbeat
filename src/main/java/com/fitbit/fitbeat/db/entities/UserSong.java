package com.fitbit.fitbeat.db.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

/**
 * @author Boris Skarabahaty.
 */
@Entity
@Table(name = "user_song")
public class UserSong {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long id;

    @NotNull
    @Column(name = "user_id")
    private long userId;

    @NotNull
    private String song;

    @NotNull
    private long playedTimes;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getSong() {
        return song;
    }

    public void setSong(String song) {
        this.song = song;
    }

    public long getPlayedTimes() {
        return playedTimes;
    }

    public void setPlayedTimes(long playedTimes) {
        this.playedTimes = playedTimes;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }
}
