package com.fitbit.fitbeat.db.repositories;

import com.fitbit.fitbeat.db.entities.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author Anton Kholodok
 */
@Transactional
public interface UserRepository extends CrudRepository<User, Long> {
    User findByFitbitId(String fitbitId);
}
