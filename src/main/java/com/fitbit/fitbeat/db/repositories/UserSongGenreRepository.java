package com.fitbit.fitbeat.db.repositories;

import com.fitbit.fitbeat.db.entities.UserSongGenre;

import org.springframework.data.repository.CrudRepository;

import java.util.List;
import javax.transaction.Transactional;

/**
 * @author Boris Skarabahaty.
 */
@Transactional
public interface UserSongGenreRepository extends CrudRepository<UserSongGenre, Long> {
    List<UserSongGenre> findByUserId(long userId);

    void deleteByUserId(long userId);
}
