package com.fitbit.fitbeat.db.repositories;

import com.fitbit.fitbeat.db.entities.UserSong;
import com.fitbit.fitbeat.db.entities.UserSongHistory;

import org.springframework.data.repository.CrudRepository;

import java.util.List;
import javax.transaction.Transactional;

/**
 * @author Boris Skarabahaty.
 */
@Transactional
public interface UserSongHistoryRepository extends CrudRepository<UserSongHistory, Long> {

    List<UserSongHistory> findByUserId(long userId);
    UserSongHistory findFirstByUserIdOrderByCreatedDesc(long userId);
}
