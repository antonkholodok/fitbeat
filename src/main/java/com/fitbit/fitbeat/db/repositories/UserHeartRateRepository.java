package com.fitbit.fitbeat.db.repositories;

import com.fitbit.fitbeat.db.entities.UserHeartRate;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * @author Anton Kholodok
 */
@Repository
public interface UserHeartRateRepository extends CrudRepository<UserHeartRate, Long> {

    UserHeartRate findByUserId(long userId);
}
