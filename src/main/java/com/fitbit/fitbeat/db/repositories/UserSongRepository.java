package com.fitbit.fitbeat.db.repositories;

import com.fitbit.fitbeat.db.entities.UserSong;

import org.springframework.data.repository.CrudRepository;

import java.util.List;
import javax.transaction.Transactional;

/**
 * @author Boris Skarabahaty.
 */
@Transactional
public interface UserSongRepository extends CrudRepository<UserSong, Long> {
    UserSong findByUserIdAndSong(long userId, String song);

    List<UserSong> findByUserId(long userId);
}
