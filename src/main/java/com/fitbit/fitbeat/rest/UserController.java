package com.fitbit.fitbeat.rest;

import com.fitbit.fitbeat.db.entities.User;
import com.fitbit.fitbeat.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Anton Kholodok
 */
@RestController
@RequestMapping("/api/user")
@CrossOrigin
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping("")
    public User user() {
        return userService.getOrCreateUser();
    }
}