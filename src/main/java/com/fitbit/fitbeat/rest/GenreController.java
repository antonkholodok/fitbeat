package com.fitbit.fitbeat.rest;

import com.fitbit.fitbeat.db.entities.User;
import com.fitbit.fitbeat.db.entities.UserSongGenre;
import com.fitbit.fitbeat.db.repositories.UserSongGenreRepository;
import com.fitbit.fitbeat.rest.dto.Genres;
import com.fitbit.fitbeat.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @author Boris Skarabahaty.
 */
@RestController
@RequestMapping(value = "/api/genres", produces = {MediaType.APPLICATION_JSON_VALUE})
@CrossOrigin
public class GenreController {
    @Autowired
    private UserService userService;

    @Autowired
    private UserSongGenreRepository songGenreRepository;

    @GetMapping
    public ResponseEntity<Genres> getGenres() {
        User user = userService.getOrCreateUser();
        List<UserSongGenre> genres = songGenreRepository.findByUserId(user.getId());
        Genres g = new Genres();
        g.setGenres(genres.stream().map(UserSongGenre::getGenre).collect(Collectors.toList()));

        return ResponseEntity.ok(g);
    }

    @PostMapping
    public ResponseEntity<?> saveGenres(@RequestBody Genres genres) {
        User user = userService.getOrCreateUser();
        songGenreRepository.deleteByUserId(user.getId());
        List<UserSongGenre> entities = genres.getGenres()
            .stream()
            .map(g -> new UserSongGenre(user.getId(), g))
            .collect(Collectors.toList());
        songGenreRepository.save(entities);

        return ResponseEntity.noContent().build();
    }
}
