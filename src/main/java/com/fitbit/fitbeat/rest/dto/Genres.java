package com.fitbit.fitbeat.rest.dto;

import java.util.Collection;

/**
 * @author Boris Skarabahaty.
 */
public class Genres {
    private Collection<String> genres;

    public Collection<String> getGenres() {
        return genres;
    }

    public void setGenres(Collection<String> genres) {
        this.genres = genres;
    }
}
