package com.fitbit.fitbeat.rest.dto;

import java.util.Objects;

/**
 * @author Boris Skarabahaty.
 */
public class StreamableSong {
    private final String id;
    private final String path;
    private final String name;
    private final int bpm;

    public StreamableSong(String path, String name, int bpm) {
        this.id = Integer.toString(path.hashCode());
        this.path = path;
        this.name = name;
        this.bpm = bpm;
    }

    public String getPath() {
        return path;
    }

    public String getName() {
        return name;
    }

    public int getBpm() {
        return bpm;
    }

    public String getId() {
        return id;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("StreamableSong{");
        sb.append("id='").append(id).append('\'');
        sb.append(", path='").append(path).append('\'');
        sb.append(", name='").append(name).append('\'');
        sb.append(", bpm=").append(bpm);
        sb.append('}');
        return sb.toString();
    }
}
