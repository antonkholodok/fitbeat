package com.fitbit.fitbeat.rest;

import com.fitbit.fitbeat.config.MusicStorageConfig;
import com.fitbit.fitbeat.db.entities.User;
import com.fitbit.fitbeat.db.entities.UserSong;
import com.fitbit.fitbeat.db.entities.UserSongHistory;
import com.fitbit.fitbeat.db.repositories.UserRepository;
import com.fitbit.fitbeat.db.repositories.UserSongHistoryRepository;
import com.fitbit.fitbeat.db.repositories.UserSongRepository;
import com.fitbit.fitbeat.service.UserService;
import com.fitbit.fitbeat.util.SongUtil;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.method.annotation.StreamingResponseBody;

import java.io.IOException;
import java.nio.channels.Channels;
import java.nio.channels.FileChannel;
import java.nio.channels.WritableByteChannel;
import java.nio.file.Path;
import java.nio.file.StandardOpenOption;
import java.util.Collections;
import java.util.Date;

/**
 * @author Boris Skarabahaty.
 */
@RestController
@RequestMapping(value = "/api/stream/{path}", produces = {StreamController.MEDIA_TYPE})
@CrossOrigin
public class StreamController {
    private static final Logger log = LoggerFactory.getLogger(StreamController.class);
    static final String MEDIA_TYPE = "audio/mpeg";
    private static final String extension = ".mp3";

    @Autowired
    private MusicStorageConfig config;

    @Autowired
    private UserSongRepository userSongRepository;

    @Autowired
    private UserSongHistoryRepository userSongHistoryRepository;

    @Autowired
    private UserService userService;

    @GetMapping
    private ResponseEntity<StreamingResponseBody> getStream(@PathVariable String path) throws IOException {
        User user = userService.getOrCreateUser();
        Path song = SongUtil.findSongByName(config.getPath(), path + extension);
        if (!song.toFile().exists()) {
            return new ResponseEntity<>(os -> {
            }, HttpStatus.NOT_FOUND);
        }

        UserSong userSong = userSongRepository.findByUserIdAndSong(user.getId(), path);
        if (userSong == null) {
            userSong = new UserSong();
            userSong.setPlayedTimes(1);
            userSong.setSong(path);
            userSong.setUserId(user.getId());
            userSong = userSongRepository.save(userSong);
        } else {
            userSong.setPlayedTimes(userSong.getPlayedTimes() + 1);
            userSong = userSongRepository.save(userSong);
        }

        UserSongHistory songHistory = new UserSongHistory();
        songHistory.setUserId(user.getId());
        songHistory.setSong(userSong.getSong());
        songHistory.setBpm(Integer.parseInt(userSong.getSong().split("_")[0]));
        songHistory.setCreated(new Date());
        userSongHistoryRepository.save(songHistory);

        FileChannel channel = FileChannel.open(song, StandardOpenOption.READ);
        StreamingResponseBody body = outputStream -> {
            WritableByteChannel out = Channels.newChannel(outputStream);
            long bytesToSend = channel.size();
            long totalSent = 0L;
            while (bytesToSend != 0) {
                long sent = channel.transferTo(totalSent, bytesToSend, out);
                bytesToSend -= sent;
                totalSent += sent;
                log.info("Sent {} from {} - {}%", totalSent, bytesToSend, totalSent * 100 / channel.size());
            }
        };

        LinkedMultiValueMap<String, String> headers = new LinkedMultiValueMap<>();
        headers.put(HttpHeaders.CONTENT_TYPE, Collections.singletonList(MEDIA_TYPE));
        headers.put(HttpHeaders.CONTENT_LENGTH, Collections.singletonList(Long.toString(channel.size())));

        return new ResponseEntity<>(body, headers, HttpStatus.OK);
    }
}
