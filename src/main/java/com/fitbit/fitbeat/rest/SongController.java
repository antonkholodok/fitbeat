package com.fitbit.fitbeat.rest;

import com.fitbit.fitbeat.config.MusicStorageConfig;
import com.fitbit.fitbeat.db.entities.User;
import com.fitbit.fitbeat.db.entities.UserSong;
import com.fitbit.fitbeat.db.repositories.UserSongGenreRepository;
import com.fitbit.fitbeat.db.repositories.UserSongRepository;
import com.fitbit.fitbeat.rest.dto.StreamableSong;
import com.fitbit.fitbeat.service.UserService;
import com.fitbit.fitbeat.util.BpmUtil;
import com.fitbit.fitbeat.util.Genre;
import com.fitbit.fitbeat.util.SongUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;

/**
 * @author Boris Skarabahaty.
 */
@RestController
@RequestMapping(value = "/api/song", produces = {MediaType.APPLICATION_JSON_VALUE})
@CrossOrigin
public class SongController {
    @Autowired
    private UserService userService;

    @Autowired
    private UserSongRepository userSongRepository;

    @Autowired
    private UserSongGenreRepository userSongGenreRepository;

    @Autowired
    private MusicStorageConfig config;

    @GetMapping("/bpm/{bpm}/search")
    public ResponseEntity<List<StreamableSong>> songsByBpm(@PathVariable String bpm) throws IOException {
        int bpmValue = BpmUtil.normalizeBpm(Integer.parseInt(bpm), config.isRoundingUp());
        return ResponseEntity.ok(songs(userService.getOrCreateUser().getId(), bpmValue));
    }

    @GetMapping("/bpm/{bpm}")
    public ResponseEntity<StreamableSong> songByBpm(@PathVariable String bpm) throws IOException {
        int bpmValue = BpmUtil.normalizeBpm(Integer.parseInt(bpm), config.isRoundingUp());
        StreamableSong song = nextSong(userService.getOrCreateUser().getId(), bpmValue);
        return song == null
            ? new ResponseEntity<>(HttpStatus.NOT_FOUND)
            : ResponseEntity.ok(song);
    }

    @GetMapping("/speed/{speed}/search")
    public ResponseEntity<List<StreamableSong>> songsBySpeed(@PathVariable String speed) throws IOException {
        int v = Integer.parseInt(speed);
        User user = userService.getOrCreateUser();
        int bpm = BpmUtil.bpmFromSpeed(v, user.getStrideLengthRunning(), config.isRoundingUp());
        return ResponseEntity.ok(songs(user.getId(), bpm));
    }

    @GetMapping("/speed/{speed}")
    public ResponseEntity<StreamableSong> songBySpeed(@PathVariable String speed) throws IOException {
        int v = Integer.parseInt(speed);
        User user = userService.getOrCreateUser();
        int bpm = BpmUtil.bpmFromSpeed(v, user.getStrideLengthRunning(), config.isRoundingUp());
        StreamableSong song = nextSong(user.getId(), bpm);
        return song == null
            ? new ResponseEntity<>(HttpStatus.NOT_FOUND)
            : ResponseEntity.ok(song);
    }

    private List<StreamableSong> songs(long userId, int bpm) {
        List<Genre> genres = userSongGenreRepository.findByUserId(userId)
            .stream()
            .map(sg -> Genre.fromValue(sg.getGenre()))
            .collect(Collectors.toList());
        return SongUtil.findSongsByBPM(config.getPath(), bpm, genres);
    }

    private StreamableSong nextSong(long userId, int bpm) {
        List<StreamableSong> candidates = songs(userId, bpm);
        if (candidates.isEmpty()) {
            return null;
        }

        Map<String, StreamableSong> candidatesMap = candidates.stream().collect(Collectors.toMap(StreamableSong::getPath, s -> s));
        Map<String, Long> playedSongs = userSongRepository.findByUserId(userId)
            .stream()
            .collect(Collectors.toMap(UserSong::getSong, UserSong::getPlayedTimes));
        Map<String, Long> compatiblePlayedSongs = new HashMap<>(candidates.size());
        candidates.forEach(c -> {
            Long playedTimes = playedSongs.get(c.getPath());
            if (playedTimes != null) {
                compatiblePlayedSongs.put(c.getPath(), playedTimes);
            }

            compatiblePlayedSongs.put(c.getPath(), 0L);
        });

        int min = compatiblePlayedSongs.values().stream().mapToInt(Long::intValue).min().orElse(0);
        List<StreamableSong> realCandidates = candidatesMap.entrySet()
            .stream()
            .filter(e -> compatiblePlayedSongs.get(e.getKey()).intValue() == min)
            .map(Map.Entry::getValue)
            .collect(Collectors.toList());
        return realCandidates.get(ThreadLocalRandom.current().nextInt(realCandidates.size()));
    }
}
