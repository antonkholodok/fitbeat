webpackJsonp([0,3],{

/***/ 110:
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__player_service__ = __webpack_require__(351);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__interfaces_Events__ = __webpack_require__(350);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__backend_service__ = __webpack_require__(71);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angular2_websocket_angular2_websocket__ = __webpack_require__(532);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_angular2_websocket_angular2_websocket___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_angular2_websocket_angular2_websocket__);
/* harmony export (binding) */ __webpack_require__.d(exports, "a", function() { return SoundManagerService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var SoundManagerService = (function () {
    function SoundManagerService(playerService, backendService) {
        var _this = this;
        this.playerService = playerService;
        this.backendService = backendService;
        this.subscribers = {};
        this.isPlaying = false;
        this.isMute = false;
        this.webSocket = new __WEBPACK_IMPORTED_MODULE_4_angular2_websocket_angular2_websocket__["$WebSocket"]('ws://159.203.173.61/hr');
        this.webSocket.getDataStream().subscribe(function (data) {
            var d = JSON.parse(data.data);
            if (d["nextSong"]) {
                _this.next();
            }
            _this.bpm = d["value"];
            _this.backendService.currentHr.next(d["value"]);
        });
        this.subscribePlayerService(this.playerService);
        // this.backendService.currentSong.subscribe((song) => {
        //   this.play(song);
        // });
    }
    SoundManagerService.prototype.play = function (song) {
        var _this = this;
        this.currentSong = song;
        this.playerService.initialize(song, function (e) {
            if (!e) {
                // this.playerService.play();
                _this.publish(__WEBPACK_IMPORTED_MODULE_2__interfaces_Events__["a" /* Events */].ChangeSong, song);
                _this.isPlaying = true;
            }
            else {
                alert(e.message);
            }
        });
    };
    SoundManagerService.prototype.togglePlayPause = function () {
        if (this.currentSong != null) {
            if (!this.isPlaying) {
                this.playerService.play();
            }
            else {
                this.playerService.pause();
            }
        }
    };
    SoundManagerService.prototype.next = function () {
        var _this = this;
        this.backendService.getSong({ bpm: this.bpm, speed: this.backendService.speed }).subscribe(function (song) {
            _this.play(song);
        });
    };
    SoundManagerService.prototype.seek = function (time) {
        if (this.playerService && this.currentSong) {
            this.playerService.seek(time);
        }
    };
    SoundManagerService.prototype.toggleMute = function () {
        if (this.currentSong) {
            if (this.isMute) {
                this.playerService.setVolume(100);
                this.isMute = false;
                this.publish(__WEBPACK_IMPORTED_MODULE_2__interfaces_Events__["a" /* Events */].Volume, false);
            }
            else {
                this.playerService.setVolume(0);
                this.isMute = true;
                this.publish(__WEBPACK_IMPORTED_MODULE_2__interfaces_Events__["a" /* Events */].Volume, true);
            }
        }
    };
    SoundManagerService.prototype.getTotalTime = function () {
        if (this.playerService && this.currentSong) {
            return this.playerService.totalTime();
        }
        return null;
    };
    SoundManagerService.prototype.on = function (event, handler) {
        if (!this.subscribers[event])
            this.subscribers[event] = [];
        this.subscribers[event].push(handler);
    };
    SoundManagerService.prototype.publish = function (event, data) {
        // console.log('Publish event:', event, data);
        if (this.subscribers[event]) {
            this.subscribers[event].forEach(function (handler) {
                handler(data);
            });
        }
    };
    SoundManagerService.prototype.getCurrentSong = function () {
        return this.currentSong;
    };
    SoundManagerService.prototype.onSongFinish = function () {
        var _this = this;
        this.backendService.getSong().subscribe(function (song) {
            _this.play(song);
        });
    };
    SoundManagerService.prototype.subscribePlayerService = function (playerService) {
        var _this = this;
        playerService.on(__WEBPACK_IMPORTED_MODULE_2__interfaces_Events__["a" /* Events */].Play, function () {
            _this.publish(__WEBPACK_IMPORTED_MODULE_2__interfaces_Events__["a" /* Events */].Play, null);
            _this.isPlaying = true;
        });
        playerService.on(__WEBPACK_IMPORTED_MODULE_2__interfaces_Events__["a" /* Events */].PlayStart, function () {
            _this.publish(__WEBPACK_IMPORTED_MODULE_2__interfaces_Events__["a" /* Events */].PlayStart, null);
            _this.isPlaying = true;
        });
        playerService.on(__WEBPACK_IMPORTED_MODULE_2__interfaces_Events__["a" /* Events */].PlayResume, function () {
            _this.publish(__WEBPACK_IMPORTED_MODULE_2__interfaces_Events__["a" /* Events */].PlayResume, null);
            _this.isPlaying = true;
        });
        playerService.on(__WEBPACK_IMPORTED_MODULE_2__interfaces_Events__["a" /* Events */].Pause, function () {
            _this.publish(__WEBPACK_IMPORTED_MODULE_2__interfaces_Events__["a" /* Events */].Pause, null);
            _this.isPlaying = false;
        });
        playerService.on(__WEBPACK_IMPORTED_MODULE_2__interfaces_Events__["a" /* Events */].Finish, function () {
            _this.publish(__WEBPACK_IMPORTED_MODULE_2__interfaces_Events__["a" /* Events */].Finish, null);
            _this.isPlaying = false;
            _this.onSongFinish();
        });
        playerService.on(__WEBPACK_IMPORTED_MODULE_2__interfaces_Events__["a" /* Events */].Seek, function () { return _this.publish(__WEBPACK_IMPORTED_MODULE_2__interfaces_Events__["a" /* Events */].Seek, null); });
        playerService.on(__WEBPACK_IMPORTED_MODULE_2__interfaces_Events__["a" /* Events */].Seeked, function () { return _this.publish(__WEBPACK_IMPORTED_MODULE_2__interfaces_Events__["a" /* Events */].Seeked, null); });
        playerService.on(__WEBPACK_IMPORTED_MODULE_2__interfaces_Events__["a" /* Events */].Time, function (time) {
            _this.publish(__WEBPACK_IMPORTED_MODULE_2__interfaces_Events__["a" /* Events */].Time, time);
        });
        playerService.on(__WEBPACK_IMPORTED_MODULE_2__interfaces_Events__["a" /* Events */].AudioError, function () {
            _this.publish(__WEBPACK_IMPORTED_MODULE_2__interfaces_Events__["a" /* Events */].AudioError, null);
            _this.isPlaying = false;
        });
        playerService.on(__WEBPACK_IMPORTED_MODULE_2__interfaces_Events__["a" /* Events */].NoStreams, function () { return _this.publish(__WEBPACK_IMPORTED_MODULE_2__interfaces_Events__["a" /* Events */].NoStreams, null); });
    };
    SoundManagerService = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__player_service__["a" /* PlayerService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__player_service__["a" /* PlayerService */]) === 'function' && _a) || Object, (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_3__backend_service__["a" /* BackendService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_3__backend_service__["a" /* BackendService */]) === 'function' && _b) || Object])
    ], SoundManagerService);
    return SoundManagerService;
    var _a, _b;
}());
//# sourceMappingURL=/Users/ustaliarou/Workspace/fitbeat/fitbeat-client/src/sound-manager.service.js.map

/***/ },

/***/ 349:
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_backend_service__ = __webpack_require__(71);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_router__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_sound_manager_service__ = __webpack_require__(110);
/* harmony export (binding) */ __webpack_require__.d(exports, "a", function() { return AppComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AppComponent = (function () {
    function AppComponent(backendService, router, soundManagerService) {
        this.backendService = backendService;
        this.router = router;
        this.soundManagerService = soundManagerService;
        this.backendService.getUser();
    }
    AppComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'body',
            template: __webpack_require__(709),
            styles: [__webpack_require__(702)]
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__services_backend_service__["a" /* BackendService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__services_backend_service__["a" /* BackendService */]) === 'function' && _a) || Object, (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* Router */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_2__angular_router__["a" /* Router */]) === 'function' && _b) || Object, (typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_3__services_sound_manager_service__["a" /* SoundManagerService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_3__services_sound_manager_service__["a" /* SoundManagerService */]) === 'function' && _c) || Object])
    ], AppComponent);
    return AppComponent;
    var _a, _b, _c;
}());
//# sourceMappingURL=/Users/ustaliarou/Workspace/fitbeat/fitbeat-client/src/app.component.js.map

/***/ },

/***/ 350:
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(exports, "a", function() { return Events; });
var Events = (function () {
    function Events() {
    }
    Events.ChangeSong = 1;
    Events.Play = 2;
    Events.PlayStart = 3;
    Events.PlayResume = 4;
    Events.Pause = 5;
    Events.Finish = 6;
    Events.Seek = 7;
    Events.Seeked = 8;
    Events.BufferingStart = 9;
    Events.BufferingEnd = 10;
    Events.AudioError = 11;
    Events.Time = 12;
    Events.NoStreams = 13;
    Events.NoProtocol = 14;
    Events.NoConnection = 15;
    Events.Volume = 16;
    return Events;
}());
//# sourceMappingURL=/Users/ustaliarou/Workspace/fitbeat/fitbeat-client/src/Events.js.map

/***/ },

/***/ 351:
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__interfaces_Events__ = __webpack_require__(350);
/* harmony export (binding) */ __webpack_require__.d(exports, "a", function() { return PlayerService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var soundManager = __webpack_require__(986).soundManager;
var PlayerService = (function () {
    function PlayerService() {
        this.subscribers = {};
        this.SERVER_API = '/api/';
    }
    PlayerService.prototype.initialize = function (song, callback) {
        var _this = this;
        if (this.lastSong) {
            soundManager.unload(this.lastSong.id);
            soundManager.destroySound(this.lastSong.id);
        }
        var soundObject = soundManager.createSound({
            url: this.SERVER_API + 'stream/' + song.path,
            id: song.id,
            volume: 100,
            onbufferchange: function () { return _this.publish(__WEBPACK_IMPORTED_MODULE_1__interfaces_Events__["a" /* Events */].BufferingStart, null); },
            ondataerror: function () { return _this.publish(__WEBPACK_IMPORTED_MODULE_1__interfaces_Events__["a" /* Events */].AudioError, null); },
            onfinish: function () { return _this.publish(__WEBPACK_IMPORTED_MODULE_1__interfaces_Events__["a" /* Events */].Finish, null); },
            onload: function () { return _this.publish(__WEBPACK_IMPORTED_MODULE_1__interfaces_Events__["a" /* Events */].BufferingStart, null); },
            onpause: function () { return _this.publish(__WEBPACK_IMPORTED_MODULE_1__interfaces_Events__["a" /* Events */].Pause, null); },
            onplay: function () { return _this.publish(__WEBPACK_IMPORTED_MODULE_1__interfaces_Events__["a" /* Events */].Play, null); },
            onresume: function () { return _this.publish(__WEBPACK_IMPORTED_MODULE_1__interfaces_Events__["a" /* Events */].PlayResume, null); },
            onstop: function () { return _this.publish(__WEBPACK_IMPORTED_MODULE_1__interfaces_Events__["a" /* Events */].Finish, null); },
            whileplaying: function () {
                var time = _this.currentTime();
                _this.publish(__WEBPACK_IMPORTED_MODULE_1__interfaces_Events__["a" /* Events */].Time, time);
            },
        });
        if (!soundObject) {
            return callback(new Error('Error while create sound'), null);
        }
        this.lastSong = song;
        soundObject.play();
        this.soundObject = soundObject;
        return callback(null, song);
    };
    PlayerService.prototype.play = function () {
        if (this.soundObject) {
            this.soundObject.resume();
        }
    };
    PlayerService.prototype.pause = function () {
        if (this.soundObject) {
            this.soundObject.pause();
        }
    };
    PlayerService.prototype.seek = function (percent) {
        if (this.soundObject) {
            var time = this.soundObject.duration * percent / 100;
            this.soundObject.setPosition(time);
        }
    };
    PlayerService.prototype.currentTime = function () {
        if (!this.soundObject)
            return;
        return this.soundObject.position;
    };
    PlayerService.prototype.totalTime = function () {
        if (!this.soundObject)
            return;
        return this.soundObject.duration;
    };
    PlayerService.prototype.setVolume = function (value) {
        if (!this.soundObject)
            return;
        this.soundObject.setVolume(value);
    };
    PlayerService.prototype.getVolume = function () {
        if (!this.soundObject)
            return;
        return this.soundObject.volume;
    };
    PlayerService.prototype.on = function (event, handler) {
        if (!this.subscribers[event])
            this.subscribers[event] = [];
        this.subscribers[event].push(handler);
    };
    PlayerService.prototype.publish = function (event, data) {
        if (this.subscribers[event]) {
            this.subscribers[event].forEach(function (handler) {
                handler(data);
            });
        }
    };
    PlayerService = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(), 
        __metadata('design:paramtypes', [])
    ], PlayerService);
    return PlayerService;
}());
//# sourceMappingURL=/Users/ustaliarou/Workspace/fitbeat/fitbeat-client/src/player.service.js.map

/***/ },

/***/ 414:
/***/ function(module, exports) {

function webpackEmptyContext(req) {
	throw new Error("Cannot find module '" + req + "'.");
}
webpackEmptyContext.keys = function() { return []; };
webpackEmptyContext.resolve = webpackEmptyContext;
module.exports = webpackEmptyContext;
webpackEmptyContext.id = 414;


/***/ },

/***/ 415:
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__polyfills_ts__ = __webpack_require__(531);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__polyfills_ts___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__polyfills_ts__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__ = __webpack_require__(495);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__environments_environment__ = __webpack_require__(530);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app___ = __webpack_require__(523);





if (__WEBPACK_IMPORTED_MODULE_3__environments_environment__["a" /* environment */].production) {
    __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2__angular_core__["enableProdMode"])();
}
__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_platform_browser_dynamic__["a" /* platformBrowserDynamic */])().bootstrapModule(__WEBPACK_IMPORTED_MODULE_4__app___["a" /* AppModule */]);
//# sourceMappingURL=/Users/ustaliarou/Workspace/fitbeat/fitbeat-client/src/main.js.map

/***/ },

/***/ 521:
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__ = __webpack_require__(152);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_forms__ = __webpack_require__(488);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_http__ = __webpack_require__(326);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__app_component__ = __webpack_require__(349);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__angular_router__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6__angular_common__ = __webpack_require__(78);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7__tags_tags_component__ = __webpack_require__(529);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8__running_running_component__ = __webpack_require__(527);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9__heart_rate_heart_rate_component__ = __webpack_require__(522);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10__services_backend_service__ = __webpack_require__(71);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11__player_player_module__ = __webpack_require__(526);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12__navigation_navigation_component__ = __webpack_require__(524);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13__search_search_component__ = __webpack_require__(528);
/* unused harmony export routes */
/* harmony export (binding) */ __webpack_require__.d(exports, "a", function() { return AppModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};














var routes = [
    { path: '', redirectTo: 'heart-rate', pathMatch: 'full' },
    { path: 'tags', component: __WEBPACK_IMPORTED_MODULE_7__tags_tags_component__["a" /* TagsComponent */] },
    { path: 'running', component: __WEBPACK_IMPORTED_MODULE_8__running_running_component__["a" /* RunningComponent */] },
    { path: 'heart-rate', component: __WEBPACK_IMPORTED_MODULE_9__heart_rate_heart_rate_component__["a" /* HeartRateComponent */] },
    { path: 'search', component: __WEBPACK_IMPORTED_MODULE_13__search_search_component__["a" /* SearchComponent */] }
];
var AppModule = (function () {
    function AppModule() {
    }
    AppModule = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_1__angular_core__["NgModule"])({
            declarations: [
                __WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* AppComponent */],
                __WEBPACK_IMPORTED_MODULE_7__tags_tags_component__["a" /* TagsComponent */],
                __WEBPACK_IMPORTED_MODULE_8__running_running_component__["a" /* RunningComponent */],
                __WEBPACK_IMPORTED_MODULE_12__navigation_navigation_component__["a" /* NavigationComponent */],
                __WEBPACK_IMPORTED_MODULE_9__heart_rate_heart_rate_component__["a" /* HeartRateComponent */],
                __WEBPACK_IMPORTED_MODULE_13__search_search_component__["a" /* SearchComponent */]
            ],
            imports: [
                __WEBPACK_IMPORTED_MODULE_6__angular_common__["b" /* CommonModule */],
                __WEBPACK_IMPORTED_MODULE_0__angular_platform_browser__["b" /* BrowserModule */],
                __WEBPACK_IMPORTED_MODULE_2__angular_forms__["a" /* FormsModule */],
                __WEBPACK_IMPORTED_MODULE_3__angular_http__["b" /* HttpModule */],
                __WEBPACK_IMPORTED_MODULE_11__player_player_module__["a" /* PlayerModule */],
                __WEBPACK_IMPORTED_MODULE_5__angular_router__["c" /* RouterModule */].forRoot(routes)
            ],
            providers: [
                __WEBPACK_IMPORTED_MODULE_10__services_backend_service__["a" /* BackendService */],
                { provide: __WEBPACK_IMPORTED_MODULE_6__angular_common__["c" /* LocationStrategy */], useClass: __WEBPACK_IMPORTED_MODULE_6__angular_common__["e" /* HashLocationStrategy */] },
                { provide: __WEBPACK_IMPORTED_MODULE_6__angular_common__["g" /* APP_BASE_HREF */], useValue: '/' }
            ],
            bootstrap: [__WEBPACK_IMPORTED_MODULE_4__app_component__["a" /* AppComponent */]]
        }), 
        __metadata('design:paramtypes', [])
    ], AppModule);
    return AppModule;
}());
//# sourceMappingURL=/Users/ustaliarou/Workspace/fitbeat/fitbeat-client/src/app.module.js.map

/***/ },

/***/ 522:
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_backend_service__ = __webpack_require__(71);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_sound_manager_service__ = __webpack_require__(110);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__angular_router__ = __webpack_require__(107);
/* harmony export (binding) */ __webpack_require__.d(exports, "a", function() { return HeartRateComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var HeartRateComponent = (function () {
    function HeartRateComponent(router, backendService, soundManagerService) {
        var _this = this;
        this.router = router;
        this.backendService = backendService;
        this.soundManagerService = soundManagerService;
        this.bpm = 80;
        backendService.currentHr.subscribe(function (value) {
            _this.bpm = value;
        });
    }
    HeartRateComponent.prototype.ngOnInit = function () {
    };
    HeartRateComponent.prototype.searchSong = function (bmp) {
        this.router.navigate(['search'], { queryParams: { bpm: this.bpm } });
    };
    HeartRateComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            host: { class: 'data' },
            selector: 'heart-rate',
            template: __webpack_require__(710),
            styles: [__webpack_require__(703)]
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_3__angular_router__["a" /* Router */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_3__angular_router__["a" /* Router */]) === 'function' && _a) || Object, (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__services_backend_service__["a" /* BackendService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__services_backend_service__["a" /* BackendService */]) === 'function' && _b) || Object, (typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__services_sound_manager_service__["a" /* SoundManagerService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_2__services_sound_manager_service__["a" /* SoundManagerService */]) === 'function' && _c) || Object])
    ], HeartRateComponent);
    return HeartRateComponent;
    var _a, _b, _c;
}());
//# sourceMappingURL=/Users/ustaliarou/Workspace/fitbeat/fitbeat-client/src/heart-rate.component.js.map

/***/ },

/***/ 523:
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__app_component__ = __webpack_require__(349);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__app_module__ = __webpack_require__(521);
/* unused harmony namespace reexport */
/* harmony namespace reexport (by used) */ __webpack_require__.d(exports, "a", function() { return __WEBPACK_IMPORTED_MODULE_1__app_module__["a"]; });


//# sourceMappingURL=/Users/ustaliarou/Workspace/fitbeat/fitbeat-client/src/index.js.map

/***/ },

/***/ 524:
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony export (binding) */ __webpack_require__.d(exports, "a", function() { return NavigationComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var NavigationComponent = (function () {
    function NavigationComponent() {
    }
    NavigationComponent.prototype.ngOnInit = function () {
    };
    NavigationComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'navigation',
            template: __webpack_require__(711),
            styles: [__webpack_require__(704)]
        }), 
        __metadata('design:paramtypes', [])
    ], NavigationComponent);
    return NavigationComponent;
}());
//# sourceMappingURL=/Users/ustaliarou/Workspace/fitbeat/fitbeat-client/src/navigation.component.js.map

/***/ },

/***/ 525:
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_sound_manager_service__ = __webpack_require__(110);
/* harmony export (binding) */ __webpack_require__.d(exports, "a", function() { return PlayerComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var PlayerComponent = (function () {
    function PlayerComponent(soundManagerService) {
        this.soundManagerService = soundManagerService;
    }
    PlayerComponent.prototype.ngOnInit = function () {
    };
    PlayerComponent.prototype.togglePlayPause = function () {
        this.soundManagerService.togglePlayPause();
    };
    PlayerComponent.prototype.next = function () {
        this.soundManagerService.next();
    };
    PlayerComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'player',
            template: __webpack_require__(712),
            styles: [__webpack_require__(705)]
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__services_sound_manager_service__["a" /* SoundManagerService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__services_sound_manager_service__["a" /* SoundManagerService */]) === 'function' && _a) || Object])
    ], PlayerComponent);
    return PlayerComponent;
    var _a;
}());
//# sourceMappingURL=/Users/ustaliarou/Workspace/fitbeat/fitbeat-client/src/player.component.js.map

/***/ },

/***/ 526:
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_common__ = __webpack_require__(78);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__player_component__ = __webpack_require__(525);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_sound_manager_service__ = __webpack_require__(110);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_player_service__ = __webpack_require__(351);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5__services_backend_service__ = __webpack_require__(71);
/* harmony export (binding) */ __webpack_require__.d(exports, "a", function() { return PlayerModule; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var PlayerModule = (function () {
    function PlayerModule() {
    }
    PlayerModule = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["NgModule"])({
            imports: [
                __WEBPACK_IMPORTED_MODULE_1__angular_common__["b" /* CommonModule */]
            ],
            declarations: [__WEBPACK_IMPORTED_MODULE_2__player_component__["a" /* PlayerComponent */]],
            providers: [__WEBPACK_IMPORTED_MODULE_3__services_sound_manager_service__["a" /* SoundManagerService */], __WEBPACK_IMPORTED_MODULE_4__services_player_service__["a" /* PlayerService */], __WEBPACK_IMPORTED_MODULE_5__services_backend_service__["a" /* BackendService */]],
            exports: [__WEBPACK_IMPORTED_MODULE_2__player_component__["a" /* PlayerComponent */]]
        }), 
        __metadata('design:paramtypes', [])
    ], PlayerModule);
    return PlayerModule;
}());
//# sourceMappingURL=/Users/ustaliarou/Workspace/fitbeat/fitbeat-client/src/player.module.js.map

/***/ },

/***/ 527:
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(107);
/* harmony export (binding) */ __webpack_require__.d(exports, "a", function() { return RunningComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var RunningComponent = (function () {
    function RunningComponent(router) {
        this.router = router;
        this.speed = 9;
    }
    RunningComponent.prototype.ngOnInit = function () {
    };
    RunningComponent.prototype.searchSong = function () {
        this.router.navigate(['search'], { queryParams: { speed: this.speed } });
    };
    RunningComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            host: { class: 'data' },
            selector: 'running',
            template: __webpack_require__(713),
            styles: [__webpack_require__(706)]
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */]) === 'function' && _a) || Object])
    ], RunningComponent);
    return RunningComponent;
    var _a;
}());
//# sourceMappingURL=/Users/ustaliarou/Workspace/fitbeat/fitbeat-client/src/running.component.js.map

/***/ },

/***/ 528:
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__angular_router__ = __webpack_require__(107);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__services_backend_service__ = __webpack_require__(71);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__services_sound_manager_service__ = __webpack_require__(110);
/* harmony export (binding) */ __webpack_require__.d(exports, "a", function() { return SearchComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SearchComponent = (function () {
    function SearchComponent(route, router, backendService, soundManagerService) {
        this.route = route;
        this.router = router;
        this.backendService = backendService;
        this.soundManagerService = soundManagerService;
        this.songsList = [];
        this.searchType = 'bpm';
        this.currentSong = null;
    }
    SearchComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.route
            .queryParams.take(1).subscribe((function (params) {
            if (params['speed']) {
                _this.searchType = 'speed';
                _this.speed = params['speed'] || '9';
                _this.searchValue = _this.speed;
                _this.backendService.lastSearchParams = { speed: _this.speed };
                _this.searchSong(_this.speed);
            }
            else {
                _this.searchType = 'bpm';
                _this.bpm = params['bpm'] || '80';
                _this.searchValue = _this.bpm;
                _this.backendService.lastSearchParams = { bpm: _this.bpm };
                _this.searchSong(_this.bpm);
            }
        }));
    };
    SearchComponent.prototype.goBack = function () {
        window.history.back();
    };
    SearchComponent.prototype.searchSong = function (searchValue) {
        var _this = this;
        this.backendService.lastSearchParams = {};
        this.backendService.lastSearchParams[this.searchType] = searchValue;
        this.backendService.getSongList(this.backendService.lastSearchParams).subscribe(function (list) {
            _this.songsList = list.map(function (songItem) {
                var names = songItem.name.split('-');
                songItem.name = names[1];
                songItem.author = names[0];
                return songItem;
            });
        });
    };
    SearchComponent.prototype.playSongs = function (startSong) {
        this.currentSong = startSong;
        this.soundManagerService.play(startSong);
        if (this.searchType === 'bpm') {
            this.router.navigate(['heart-rate']);
        }
        else {
            this.router.navigate(['running']);
        }
    };
    SearchComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'app-search',
            template: __webpack_require__(714),
            styles: [__webpack_require__(707)]
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* ActivatedRoute */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__angular_router__["b" /* ActivatedRoute */]) === 'function' && _a) || Object, (typeof (_b = typeof __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__angular_router__["a" /* Router */]) === 'function' && _b) || Object, (typeof (_c = typeof __WEBPACK_IMPORTED_MODULE_2__services_backend_service__["a" /* BackendService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_2__services_backend_service__["a" /* BackendService */]) === 'function' && _c) || Object, (typeof (_d = typeof __WEBPACK_IMPORTED_MODULE_3__services_sound_manager_service__["a" /* SoundManagerService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_3__services_sound_manager_service__["a" /* SoundManagerService */]) === 'function' && _d) || Object])
    ], SearchComponent);
    return SearchComponent;
    var _a, _b, _c, _d;
}());
//# sourceMappingURL=/Users/ustaliarou/Workspace/fitbeat/fitbeat-client/src/search.component.js.map

/***/ },

/***/ 529:
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__services_backend_service__ = __webpack_require__(71);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_lodash__ = __webpack_require__(700);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_lodash___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_lodash__);
/* harmony export (binding) */ __webpack_require__.d(exports, "a", function() { return TagsComponent; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var TagsComponent = (function () {
    function TagsComponent(backendService) {
        this.backendService = backendService;
    }
    TagsComponent.prototype.ngOnInit = function () {
        this.backendService.getGenres().subscribe(function (genres) {
        });
    };
    TagsComponent.prototype.onChangeCheckbox = function (event, form) {
        var genresResponse = { genres: Object.keys(__webpack_require__.i(__WEBPACK_IMPORTED_MODULE_2_lodash__["omitBy"])(form.value, function (val) { return !val; })) };
        this.backendService.postGenres(genresResponse);
    };
    TagsComponent = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Component"])({
            selector: 'tags',
            template: __webpack_require__(715),
            styles: [__webpack_require__(708)]
        }), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_1__services_backend_service__["a" /* BackendService */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_1__services_backend_service__["a" /* BackendService */]) === 'function' && _a) || Object])
    ], TagsComponent);
    return TagsComponent;
    var _a;
}());
//# sourceMappingURL=/Users/ustaliarou/Workspace/fitbeat/fitbeat-client/src/tags.component.js.map

/***/ },

/***/ 530:
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony export (binding) */ __webpack_require__.d(exports, "a", function() { return environment; });
// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `angular-cli.json`.
var environment = {
    production: false
};
//# sourceMappingURL=/Users/ustaliarou/Workspace/fitbeat/fitbeat-client/src/environment.js.map

/***/ },

/***/ 531:
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_core_js_es6_symbol__ = __webpack_require__(546);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0_core_js_es6_symbol___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0_core_js_es6_symbol__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_core_js_es6_object__ = __webpack_require__(539);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_core_js_es6_object___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_core_js_es6_object__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_core_js_es6_function__ = __webpack_require__(535);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2_core_js_es6_function___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2_core_js_es6_function__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_core_js_es6_parse_int__ = __webpack_require__(541);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3_core_js_es6_parse_int___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3_core_js_es6_parse_int__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_core_js_es6_parse_float__ = __webpack_require__(540);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4_core_js_es6_parse_float___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_4_core_js_es6_parse_float__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_core_js_es6_number__ = __webpack_require__(538);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_5_core_js_es6_number___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_5_core_js_es6_number__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_core_js_es6_math__ = __webpack_require__(537);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_6_core_js_es6_math___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_6_core_js_es6_math__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_core_js_es6_string__ = __webpack_require__(545);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_7_core_js_es6_string___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_7_core_js_es6_string__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_core_js_es6_date__ = __webpack_require__(534);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_8_core_js_es6_date___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_8_core_js_es6_date__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_core_js_es6_array__ = __webpack_require__(533);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_9_core_js_es6_array___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_9_core_js_es6_array__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_core_js_es6_regexp__ = __webpack_require__(543);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_10_core_js_es6_regexp___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_10_core_js_es6_regexp__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_core_js_es6_map__ = __webpack_require__(536);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_11_core_js_es6_map___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_11_core_js_es6_map__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_core_js_es6_set__ = __webpack_require__(544);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_12_core_js_es6_set___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_12_core_js_es6_set__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13_core_js_es6_reflect__ = __webpack_require__(542);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_13_core_js_es6_reflect___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_13_core_js_es6_reflect__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14_core_js_es7_reflect__ = __webpack_require__(547);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_14_core_js_es7_reflect___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_14_core_js_es7_reflect__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15_zone_js_dist_zone__ = __webpack_require__(988);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_15_zone_js_dist_zone___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_15_zone_js_dist_zone__);
















//# sourceMappingURL=/Users/ustaliarou/Workspace/fitbeat/fitbeat-client/src/polyfills.js.map

/***/ },

/***/ 702:
/***/ function(module, exports) {

module.exports = ""

/***/ },

/***/ 703:
/***/ function(module, exports) {

module.exports = ""

/***/ },

/***/ 704:
/***/ function(module, exports) {

module.exports = ""

/***/ },

/***/ 705:
/***/ function(module, exports) {

module.exports = ""

/***/ },

/***/ 706:
/***/ function(module, exports) {

module.exports = ""

/***/ },

/***/ 707:
/***/ function(module, exports) {

module.exports = ""

/***/ },

/***/ 708:
/***/ function(module, exports) {

module.exports = ""

/***/ },

/***/ 709:
/***/ function(module, exports) {

module.exports = "<navigation *ngIf=\"!router.isActive('search')\"></navigation>\n<router-outlet></router-outlet>\n<player *ngIf=\"!router.isActive('search') && soundManagerService.currentSong\"></player>\n"

/***/ },

/***/ 71:
/***/ function(module, exports, __webpack_require__) {

"use strict";
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__angular_core__ = __webpack_require__(1);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs__ = __webpack_require__(718);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1_rxjs___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1_rxjs__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__angular_http__ = __webpack_require__(326);
/* harmony export (binding) */ __webpack_require__.d(exports, "a", function() { return BackendService; });
var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (this && this.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var BackendService = (function () {
    function BackendService(http) {
        this.http = http;
        this.SERVER_API = '/api/';
        this.currentGenres = {};
        this.allGenres = ['Rock', 'Metal', 'Electronic', 'Country', 'Pop'];
        this.lastSearchParams = {};
        this.currentSong = null;
        this.currentHr = new __WEBPACK_IMPORTED_MODULE_1_rxjs__["BehaviorSubject"](80);
    }
    BackendService.prototype.getSong = function (searchParams) {
        var _this = this;
        if (searchParams) {
            this.lastSearchParams = searchParams;
        }
        if (searchParams.bpm) {
            return this.http.get(this.SERVER_API + 'song/bpm/' + this.lastSearchParams.bpm).map(function (response) {
                var songItem = response.json();
                var names = songItem.name.split('-');
                songItem.name = names[1];
                songItem.author = names[0];
                _this.currentSong = songItem;
                return _this.currentSong;
            });
        }
        else {
            this.speed = this.lastSearchParams.speed;
            return this.http.get(this.SERVER_API + 'song/speed/' + this.lastSearchParams.speed).map(function (response) {
                var songItem = response.json();
                var names = songItem.name.split('-');
                songItem.name = names[1];
                songItem.author = names[0];
                _this.currentSong = songItem;
                return _this.currentSong;
            });
        }
    };
    BackendService.prototype.getGenres = function () {
        var _this = this;
        return this.http.get(this.SERVER_API + 'genres').map(function (response) {
            var genres = response.json().genres;
            if (genres.includes('All')) {
                _this.currentGenres = _this.allGenres.reduce(function (accumulator, genre) {
                    accumulator[genre] = true;
                    return accumulator;
                }, {});
            }
            else {
                _this.currentGenres = genres.reduce(function (accumulator, genre) {
                    accumulator[genre] = true;
                    return accumulator;
                }, {});
            }
            return _this.currentGenres;
        });
    };
    BackendService.prototype.postGenres = function (genresResponse) {
        this.http.post(this.SERVER_API + 'genres', genresResponse).subscribe(function (res) {
            console.log(res.json());
        });
    };
    BackendService.prototype.getUser = function () {
        this.http.get(this.SERVER_API + 'user').subscribe(function (response) {
            console.log(response.json());
        });
    };
    BackendService.prototype.getSongList = function (_a) {
        var bpm = _a.bpm, speed = _a.speed;
        if (bpm) {
            return this.http.get(this.SERVER_API + ("song/bpm/" + bpm + "/search")).map(function (response) {
                return response.json();
            });
        }
        if (speed) {
            this.speed = speed;
            return this.http.get(this.SERVER_API + ("song/speed/" + speed + "/search")).map(function (response) {
                return response.json();
            });
        }
    };
    BackendService = __decorate([
        __webpack_require__.i(__WEBPACK_IMPORTED_MODULE_0__angular_core__["Injectable"])(), 
        __metadata('design:paramtypes', [(typeof (_a = typeof __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Http */] !== 'undefined' && __WEBPACK_IMPORTED_MODULE_2__angular_http__["a" /* Http */]) === 'function' && _a) || Object])
    ], BackendService);
    return BackendService;
    var _a;
}());
//# sourceMappingURL=/Users/ustaliarou/Workspace/fitbeat/fitbeat-client/src/backend.service.js.map

/***/ },

/***/ 710:
/***/ function(module, exports) {

module.exports = "<div class=\"block\">\n  <div class=\"title\">{{bpm}}</div>\n  <div class=\"icon_and_units\">\n    <div class=\"icon\"></div>\n    <div class=\"units\">BPM</div>\n  </div>\n</div>\n<button (click)=\"searchSong()\" class=\"search_button\"></button>\n"

/***/ },

/***/ 711:
/***/ function(module, exports) {

module.exports = "<a class=\"heart_rate\" routerLinkActive=\"active\" [routerLink]=\"['./heart-rate']\"></a>\n<a class=\"running\" routerLinkActive=\"active\" [routerLink]=\"['./running']\"></a>\n<a class=\"tags\" routerLinkActive=\"active\" [routerLink]=\"['./tags']\"></a>\n"

/***/ },

/***/ 712:
/***/ function(module, exports) {

module.exports = "<div class=\"player\">\n  <div class=\"titles\">\n    <div class=\"title_of_song\">{{soundManagerService.currentSong?.name}}</div>\n    <div class=\"title_of_band\">{{soundManagerService.currentSong?.author}}</div>\n    <div class=\"bpm_of_song\">{{soundManagerService.currentSong?.bpm}} BPM</div>\n  </div>\n  <div class=\"player_buttons\">\n    <button [ngClass]=\"{pause: soundManagerService.isPlaying}\" class=\"play_and_pause\" (click)=\"togglePlayPause()\"></button>\n    <button class=\"next_track\" (click)=\"next()\"></button>\n  </div>\n</div>\n"

/***/ },

/***/ 713:
/***/ function(module, exports) {

module.exports = "<div class=\"block\">\n  <div class=\"title\">{{speed}}</div>\n  <div class=\"icon_and_units\">\n    <div class=\"icon steps\"></div>\n    <div class=\"units\">Speed</div>\n  </div>\n</div>\n<button (click)=\"searchSong()\" class=\"search_button\"></button>\n"

/***/ },

/***/ 714:
/***/ function(module, exports) {

module.exports = "<div class=\"formSearch\">\n  <button (click)=\"goBack()\" class=\"back\"></button>\n  <input (keyup.enter)=\"searchSong(searchValue)\" name=\"searchInput\" [(ngModel)]=\"searchValue\" class=\"inputBpmOrKm\"/>\n  <label class=\"labelBpmOrKm\">{{searchType}}</label>\n</div>\n<ul class=\"search_snippets\">\n  <li (click)=playSongs(song) *ngFor=\"let song of songsList\" class=\"search_snippet\">\n    <div class=\"title_of_song search_result_snippet_width_of_label\">{{song.name}}</div>\n    <div class=\"title_of_band\">{{song.author}}</div>\n    <div class=\"bpm_of_song\">{{song.bpm}}</div>\n  </li>\n</ul>\n"

/***/ },

/***/ 715:
/***/ function(module, exports) {

module.exports = "<form #f=\"ngForm\">\n  <ul class=\"genreRows\">\n    <li *ngFor=\"let genre of backendService.allGenres\" class=\"gengeRow\">\n      <div>{{genre}}</div>\n      <input name=\"{{genre}}\" (change)=\"onChangeCheckbox($event, f)\" [(ngModel)]=\"backendService.currentGenres[genre]\" type=\"checkbox\"/>\n      <span></span>\n    </li>\n  </ul>\n</form>\n"

/***/ },

/***/ 989:
/***/ function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(415);


/***/ }

},[989]);
//# sourceMappingURL=main.bundle.map