webpackJsonp([1,3],{

/***/ 416:
/***/ function(module, exports, __webpack_require__) {

// style-loader: Adds some css to the DOM by adding a <style> tag

// load the styles
var content = __webpack_require__(683);
if(typeof content === 'string') content = [[module.i, content, '']];
// add the styles to the DOM
var update = __webpack_require__(987)(content, {});
if(content.locals) module.exports = content.locals;
// Hot Module Replacement
if(false) {
	// When the styles change, update the <style> tags
	if(!content.locals) {
		module.hot.accept("!!./../node_modules/css-loader/index.js?sourcemap!./../node_modules/postcss-loader/index.js!./styles.css", function() {
			var newContent = require("!!./../node_modules/css-loader/index.js?sourcemap!./../node_modules/postcss-loader/index.js!./styles.css");
			if(typeof newContent === 'string') newContent = [[module.id, newContent, '']];
			update(newContent);
		});
	}
	// When the module is disposed, remove the <style> tags
	module.hot.dispose(function() { update(); });
}

/***/ },

/***/ 683:
/***/ function(module, exports, __webpack_require__) {

exports = module.exports = __webpack_require__(684)();
// imports


// module
exports.push([module.i, "* {\n  -webkit-transition: all 0.4s;\n  transition: all 0.4s;\n  box-sizing: border-box;\n}\n\nrouter-outlet {\n  display: none;\n}\n\nbody {\n  padding: 0;\n  margin: 0;\n  width: 100vw;\n  height: 100vh;\n  color: #424c62;\n  font-family: -apple-system, BlinkMacSystemFont, \"San Francisco\", Helvetica, Arial, sans-serif;\n}\n\nnavigation {\n  width: 100vw;\n  height: 72px;\n  display: -ms-flexbox;\n  display: -webkit-box;\n  display: flex;\n  -ms-flex-pack: justify;\n  -webkit-box-pack: justify;\n          justify-content: space-between;\n  -ms-flex-align: center;\n  -webkit-box-align: center;\n          align-items: center;\n  background: #fff;\n  border-bottom: 1px solid #ecedef;\n  top: 0;\n  position: absolute;\n}\n\nnavigation a {\n  width: 33.33vw;\n  height: 100%;\n}\n\n.heart_rate {\n  background-image: url(" + __webpack_require__(689) + ");\n  background-repeat: no-repeat;\n  background-position: center;\n}\n\n.heart_rate.active {\n  background-image: url(" + __webpack_require__(690) + ");\n}\n\n.running {\n  background-image: url(" + __webpack_require__(694) + ");\n  background-repeat: no-repeat;\n  background-position: center;\n}\n\n.running.active {\n  background-image: url(" + __webpack_require__(695) + ");\n}\n\n.tags {\n  background-image: url(" + __webpack_require__(698) + ");\n  background-repeat: no-repeat;\n  background-position: center;\n}\n\n.tags.active {\n  background-image: url(" + __webpack_require__(699) + ");\n}\n\n.data {\n  background: #fff;\n  width: 100vw;\n  height: 260px;\n  display: -ms-flexbox;\n  display: -webkit-box;\n  display: flex;\n  -ms-flex-direction: column;\n  -webkit-box-orient: vertical;\n  -webkit-box-direction: normal;\n          flex-direction: column;\n  -ms-flex-pack: justify;\n  -webkit-box-pack: justify;\n          justify-content: space-between;\n  -ms-flex-align: center;\n  -webkit-box-align: center;\n          align-items: center;\n  margin-top: -36px;\n  top: 50%;\n  position: absolute;\n  margin-top: -166px;\n}\n\n.block {\n  display: -ms-flexbox;\n  display: -webkit-box;\n  display: flex;\n  -ms-flex-pack: center;\n  -webkit-box-pack: center;\n          justify-content: center;\n}\n\n.title {\n  font-size: 160px;\n  font-weight: 100;\n  text-align: right;\n  padding: 0 8px;\n}\n\n.icon_and_units {\n  padding: 0 8px;\n}\n\n.icon {\n  width: 56px;\n  height: 56px;\n  background: url(" + __webpack_require__(688) + ");\n  background-repeat: no-repeat;\n  margin-top: 36px;\n  -webkit-animation: heartAnime 1s ease infinite;\n  animation: heartAnime 1s ease infinite;\n}\n\n.icon.steps {\n  background: url(" + __webpack_require__(697) + ");\n}\n\n.units {\n  font-size: 20px;\n  margin-top: 42px;\n  letter-spacing: 2px;\n  text-align: center;\n}\n\n.search_button {\n  height: 56px;\n  width: 264px;\n  background: #4dbfbf url(" + __webpack_require__(696) + ") no-repeat center;\n  border-radius: 28px;\n}\n\n.player {\n  background: #fff;\n  border-top: 1px solid #ecedef;\n  width: 100vw;\n  height: 96px;\n  display: -ms-flexbox;\n  display: -webkit-box;\n  display: flex;\n  -ms-flex-pack: justify;\n  -webkit-box-pack: justify;\n          justify-content: space-between;\n  -ms-flex-align: center;\n  -webkit-box-align: center;\n          align-items: center;\n  position: absolute;\n  bottom: 0\n}\n\n.titles {\n  padding-left: 24px;\n}\n\n.title_of_song {\n  font-size: 14px;\n  width: 160px;\n  overflow: hidden;\n  text-overflow: ellipsis;\n  white-space: nowrap;\n  border: 0px;\n  line-height: 20px;\n}\n\n.title_of_band {\n  font-size: 14px;\n  opacity: 0.5;\n  line-height: 20px;\n  font-weight: 600;\n}\n\n.bpm_of_song {\n  font-size: 12px;\n  line-height: 20px;\n  opacity: 0.5;\n}\n\n.player_buttons {\n  display: -ms-flexbox;\n  display: -webkit-box;\n  display: flex;\n  -ms-flex-pack: justify;\n  -webkit-box-pack: justify;\n          justify-content: space-between;\n  -ms-flex-align: center;\n  -webkit-box-align: center;\n          align-items: center;\n  height: 48px;\n  padding-right: 16px;\n}\n\n.play_and_pause {\n  width: 48px;\n  height: 48px;\n  background: url(" + __webpack_require__(693) + ");\n  margin: 8px;\n}\n\n.play_and_pause.pause {\n  background: url(" + __webpack_require__(692) + ");\n}\n\n.next_track {\n  width: 32px;\n  height: 32px;\n  background: url(" + __webpack_require__(691) + ");\n  background-repeat: no-repeat;\n  margin: 8px;\n}\n\nbutton {\n  border: none;\n}\n\nbutton:hover {\n  border: none;\n  box-shadow: none;\n}\n\n@-webkit-keyframes heartAnime {\n  0% {\n    -webkit-transform: scale(1);\n    transform: scale(1);\n  }\n  50% {\n    -webkit-transform: scale(0.8);\n    transform: scale(0.8);\n  }\n  100% {\n    -webkit-transform: scale(1);\n    transform: scale(1);\n  }\n}\n\n@keyframes heartAnime {\n  0% {\n    -webkit-transform: scale(1);\n    transform: scale(1);\n  }\n  50% {\n    -webkit-transform: scale(0.8);\n    transform: scale(0.8);\n  }\n  100% {\n    -webkit-transform: scale(1);\n    transform: scale(1);\n  }\n}\n\n\n.gengeRow {\n  list-style: none;\n  height: 72px;\n  line-height: 72px;\n  padding-left: 24px;\n  font-size: 16px;\n  border-bottom: 0.5px solid #ecedef;\n}\n\n.genreRows {\n  margin: 0px;\n  padding: 0px;\n  margin-top: 72px;\n}\n\n.search_snippet {\n  height: 96px;\n  border-bottom: 0.5px solid #ecedef;\n  margin: 0px;\n  padding: 0px;\n  padding-left: 24px;\n  padding-top: 17px;\n}\n\n.search_snippets {\n  margin: 0px;\n  padding: 0px;\n}\n\n.gengeRow input {\n  /*display: none;*/\n  opacity: 0;\n  position: absolute;\n  margin-top: -72px;\n  width: 100vw;\n  height: 72px;\n}\n\n\n.gengeRow span {\n    width: 20px;\n    height: 20px;\n    display: block;\n    background: url(" + __webpack_require__(686) + ");\n    position: absolute;\n    right: 24px;\n    margin-top: -46px;\n}\n\n.gengeRow input:checked + span {\n  background: url(" + __webpack_require__(687) + ");\n  position: absolute;\n}\n\n.back {\n  width: 72px;\n  height: 72px;\n  background: url(" + __webpack_require__(685) + ");\n  background-repeat: no-repeat;\n  background-position: center;\n\n}\n\n.inputBpmOrKm {\n  font-size: 24px;\n  font-weight: 300;\n  position: absolute;\n  margin-top: 18px;\n  width: 50px;\n}\n\n\n.formSearch {\n  border-bottom: 1px solid #ecedef;\n}\n\n.labelBpmOrKm {\n  font-size: 24px;\n  font-weight: 300;\n  opacity: .5;\n  position: absolute;\n  margin-top: 21px;\n  margin-left: 48px;\n}\n\n.search_result_snippet_width_of_label {\n  width: 320px;\n}\n", ""]);

// exports


/***/ },

/***/ 684:
/***/ function(module, exports) {

/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/
// css base code, injected by the css-loader
module.exports = function() {
	var list = [];

	// return the list of modules as css string
	list.toString = function toString() {
		var result = [];
		for(var i = 0; i < this.length; i++) {
			var item = this[i];
			if(item[2]) {
				result.push("@media " + item[2] + "{" + item[1] + "}");
			} else {
				result.push(item[1]);
			}
		}
		return result.join("");
	};

	// import a list of modules into the list
	list.i = function(modules, mediaQuery) {
		if(typeof modules === "string")
			modules = [[null, modules, ""]];
		var alreadyImportedModules = {};
		for(var i = 0; i < this.length; i++) {
			var id = this[i][0];
			if(typeof id === "number")
				alreadyImportedModules[id] = true;
		}
		for(i = 0; i < modules.length; i++) {
			var item = modules[i];
			// skip already imported module
			// this implementation is not 100% perfect for weird media query combinations
			//  when a module is imported multiple times with different media queries.
			//  I hope this will never occur (Hey this way we have smaller bundles)
			if(typeof item[0] !== "number" || !alreadyImportedModules[item[0]]) {
				if(mediaQuery && !item[2]) {
					item[2] = mediaQuery;
				} else if(mediaQuery) {
					item[2] = "(" + item[2] + ") and (" + mediaQuery + ")";
				}
				list.push(item);
			}
		}
	};
	return list;
};


/***/ },

/***/ 685:
/***/ function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "e1f7f1b766cc2f3e5c2b8b0b27c05894.svg";

/***/ },

/***/ 686:
/***/ function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "01d263f185cb28dae4eb9077888e6027.svg";

/***/ },

/***/ 687:
/***/ function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "b7f2fb93847b30452f0ce233e9b1ef80.svg";

/***/ },

/***/ 688:
/***/ function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "d1caa9c18f95d45f490879296d06d9ef.svg";

/***/ },

/***/ 689:
/***/ function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "946cde4e4534a5cc780111d952dedcf8.svg";

/***/ },

/***/ 690:
/***/ function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "8293420eb6c0b1ee55424ef99a5e2177.svg";

/***/ },

/***/ 691:
/***/ function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "45d80056a69238fd0242b6a0b0522a22.svg";

/***/ },

/***/ 692:
/***/ function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "c379625c7c89587a22c9772be94eb77f.svg";

/***/ },

/***/ 693:
/***/ function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "a72f660714df5b423b44d128c8c96020.svg";

/***/ },

/***/ 694:
/***/ function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "69277bd895c7c587bac2f431be5af95e.svg";

/***/ },

/***/ 695:
/***/ function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "91ea6b92edd6ce7049e27744ee918b5a.svg";

/***/ },

/***/ 696:
/***/ function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "85d3eb850b0967fa75ba7ae959984f53.svg";

/***/ },

/***/ 697:
/***/ function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "d1d8c3c51c894ca93f6a2fc800435713.svg";

/***/ },

/***/ 698:
/***/ function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "75d72a55b0a68aeeb3b77a533eb4cd09.svg";

/***/ },

/***/ 699:
/***/ function(module, exports, __webpack_require__) {

module.exports = __webpack_require__.p + "1110bfda83f7c896eb8e5851a2bfca34.svg";

/***/ },

/***/ 987:
/***/ function(module, exports) {

/*
	MIT License http://www.opensource.org/licenses/mit-license.php
	Author Tobias Koppers @sokra
*/
var stylesInDom = {},
	memoize = function(fn) {
		var memo;
		return function () {
			if (typeof memo === "undefined") memo = fn.apply(this, arguments);
			return memo;
		};
	},
	isOldIE = memoize(function() {
		return /msie [6-9]\b/.test(window.navigator.userAgent.toLowerCase());
	}),
	getHeadElement = memoize(function () {
		return document.head || document.getElementsByTagName("head")[0];
	}),
	singletonElement = null,
	singletonCounter = 0,
	styleElementsInsertedAtTop = [];

module.exports = function(list, options) {
	if(typeof DEBUG !== "undefined" && DEBUG) {
		if(typeof document !== "object") throw new Error("The style-loader cannot be used in a non-browser environment");
	}

	options = options || {};
	// Force single-tag solution on IE6-9, which has a hard limit on the # of <style>
	// tags it will allow on a page
	if (typeof options.singleton === "undefined") options.singleton = isOldIE();

	// By default, add <style> tags to the bottom of <head>.
	if (typeof options.insertAt === "undefined") options.insertAt = "bottom";

	var styles = listToStyles(list);
	addStylesToDom(styles, options);

	return function update(newList) {
		var mayRemove = [];
		for(var i = 0; i < styles.length; i++) {
			var item = styles[i];
			var domStyle = stylesInDom[item.id];
			domStyle.refs--;
			mayRemove.push(domStyle);
		}
		if(newList) {
			var newStyles = listToStyles(newList);
			addStylesToDom(newStyles, options);
		}
		for(var i = 0; i < mayRemove.length; i++) {
			var domStyle = mayRemove[i];
			if(domStyle.refs === 0) {
				for(var j = 0; j < domStyle.parts.length; j++)
					domStyle.parts[j]();
				delete stylesInDom[domStyle.id];
			}
		}
	};
}

function addStylesToDom(styles, options) {
	for(var i = 0; i < styles.length; i++) {
		var item = styles[i];
		var domStyle = stylesInDom[item.id];
		if(domStyle) {
			domStyle.refs++;
			for(var j = 0; j < domStyle.parts.length; j++) {
				domStyle.parts[j](item.parts[j]);
			}
			for(; j < item.parts.length; j++) {
				domStyle.parts.push(addStyle(item.parts[j], options));
			}
		} else {
			var parts = [];
			for(var j = 0; j < item.parts.length; j++) {
				parts.push(addStyle(item.parts[j], options));
			}
			stylesInDom[item.id] = {id: item.id, refs: 1, parts: parts};
		}
	}
}

function listToStyles(list) {
	var styles = [];
	var newStyles = {};
	for(var i = 0; i < list.length; i++) {
		var item = list[i];
		var id = item[0];
		var css = item[1];
		var media = item[2];
		var sourceMap = item[3];
		var part = {css: css, media: media, sourceMap: sourceMap};
		if(!newStyles[id])
			styles.push(newStyles[id] = {id: id, parts: [part]});
		else
			newStyles[id].parts.push(part);
	}
	return styles;
}

function insertStyleElement(options, styleElement) {
	var head = getHeadElement();
	var lastStyleElementInsertedAtTop = styleElementsInsertedAtTop[styleElementsInsertedAtTop.length - 1];
	if (options.insertAt === "top") {
		if(!lastStyleElementInsertedAtTop) {
			head.insertBefore(styleElement, head.firstChild);
		} else if(lastStyleElementInsertedAtTop.nextSibling) {
			head.insertBefore(styleElement, lastStyleElementInsertedAtTop.nextSibling);
		} else {
			head.appendChild(styleElement);
		}
		styleElementsInsertedAtTop.push(styleElement);
	} else if (options.insertAt === "bottom") {
		head.appendChild(styleElement);
	} else {
		throw new Error("Invalid value for parameter 'insertAt'. Must be 'top' or 'bottom'.");
	}
}

function removeStyleElement(styleElement) {
	styleElement.parentNode.removeChild(styleElement);
	var idx = styleElementsInsertedAtTop.indexOf(styleElement);
	if(idx >= 0) {
		styleElementsInsertedAtTop.splice(idx, 1);
	}
}

function createStyleElement(options) {
	var styleElement = document.createElement("style");
	styleElement.type = "text/css";
	insertStyleElement(options, styleElement);
	return styleElement;
}

function createLinkElement(options) {
	var linkElement = document.createElement("link");
	linkElement.rel = "stylesheet";
	insertStyleElement(options, linkElement);
	return linkElement;
}

function addStyle(obj, options) {
	var styleElement, update, remove;

	if (options.singleton) {
		var styleIndex = singletonCounter++;
		styleElement = singletonElement || (singletonElement = createStyleElement(options));
		update = applyToSingletonTag.bind(null, styleElement, styleIndex, false);
		remove = applyToSingletonTag.bind(null, styleElement, styleIndex, true);
	} else if(obj.sourceMap &&
		typeof URL === "function" &&
		typeof URL.createObjectURL === "function" &&
		typeof URL.revokeObjectURL === "function" &&
		typeof Blob === "function" &&
		typeof btoa === "function") {
		styleElement = createLinkElement(options);
		update = updateLink.bind(null, styleElement);
		remove = function() {
			removeStyleElement(styleElement);
			if(styleElement.href)
				URL.revokeObjectURL(styleElement.href);
		};
	} else {
		styleElement = createStyleElement(options);
		update = applyToTag.bind(null, styleElement);
		remove = function() {
			removeStyleElement(styleElement);
		};
	}

	update(obj);

	return function updateStyle(newObj) {
		if(newObj) {
			if(newObj.css === obj.css && newObj.media === obj.media && newObj.sourceMap === obj.sourceMap)
				return;
			update(obj = newObj);
		} else {
			remove();
		}
	};
}

var replaceText = (function () {
	var textStore = [];

	return function (index, replacement) {
		textStore[index] = replacement;
		return textStore.filter(Boolean).join('\n');
	};
})();

function applyToSingletonTag(styleElement, index, remove, obj) {
	var css = remove ? "" : obj.css;

	if (styleElement.styleSheet) {
		styleElement.styleSheet.cssText = replaceText(index, css);
	} else {
		var cssNode = document.createTextNode(css);
		var childNodes = styleElement.childNodes;
		if (childNodes[index]) styleElement.removeChild(childNodes[index]);
		if (childNodes.length) {
			styleElement.insertBefore(cssNode, childNodes[index]);
		} else {
			styleElement.appendChild(cssNode);
		}
	}
}

function applyToTag(styleElement, obj) {
	var css = obj.css;
	var media = obj.media;

	if(media) {
		styleElement.setAttribute("media", media)
	}

	if(styleElement.styleSheet) {
		styleElement.styleSheet.cssText = css;
	} else {
		while(styleElement.firstChild) {
			styleElement.removeChild(styleElement.firstChild);
		}
		styleElement.appendChild(document.createTextNode(css));
	}
}

function updateLink(linkElement, obj) {
	var css = obj.css;
	var sourceMap = obj.sourceMap;

	if(sourceMap) {
		// http://stackoverflow.com/a/26603875
		css += "\n/*# sourceMappingURL=data:application/json;base64," + btoa(unescape(encodeURIComponent(JSON.stringify(sourceMap)))) + " */";
	}

	var blob = new Blob([css], { type: "text/css" });

	var oldSrc = linkElement.href;

	linkElement.href = URL.createObjectURL(blob);

	if(oldSrc)
		URL.revokeObjectURL(oldSrc);
}


/***/ },

/***/ 990:
/***/ function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(416);


/***/ }

},[990]);
//# sourceMappingURL=styles.bundle.map