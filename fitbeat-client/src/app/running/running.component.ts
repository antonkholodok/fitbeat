import { Component, OnInit } from '@angular/core';
import {Router} from "@angular/router";

@Component({
  host: {class: 'data'},
  selector: 'running',
  templateUrl: './running.component.html',
  styleUrls: ['./running.component.css']
})
export class RunningComponent implements OnInit {

  public speed = 9;

  constructor(private router : Router) { }

  ngOnInit() {
  }

  searchSong() {
    this.router.navigate(['search'], {queryParams: {speed: this.speed}});
  }

}
