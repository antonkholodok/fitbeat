import {Injectable} from '@angular/core';

import {Subject, Observable, BehaviorSubject} from 'rxjs';

import {Http} from "@angular/http";

@Injectable()
export class BackendService {

  private SERVER_API = '/api/';


  public currentGenres = {};

  public allGenres = ['Rock', 'Metal', 'Electronic', 'Country', 'Pop'];

  public lastSearchParams: any = {};

  currentSong = null;

  speed;

  currentHr: Subject<number> = new BehaviorSubject<number>(80);

  constructor(private http: Http) {

  }

  getSong(searchParams?) {
    if (searchParams) {
      this.lastSearchParams = searchParams;
    }
    if (searchParams.bpm) {
      return this.http.get(this.SERVER_API + 'song/bpm/' + this.lastSearchParams.bpm).map((response) => {
        const songItem = response.json();
        const names = songItem.name.split('-');
        songItem.name = names[1];
        songItem.author = names[0];
        this.currentSong = songItem;
        return this.currentSong;
      });
    } else {
      this.speed = this.lastSearchParams.speed;
      return this.http.get(this.SERVER_API + 'song/speed/' + this.lastSearchParams.speed).map((response) => {
        const songItem = response.json();
        const names = songItem.name.split('-');
        songItem.name = names[1];
        songItem.author = names[0];
        this.currentSong = songItem;
        return this.currentSong;
      });
    }
  }

  getGenres() {
    return this.http.get(this.SERVER_API + 'genres').map((response) => {
      const genres = response.json().genres;
      if (genres.includes('All')) {
        this.currentGenres = this.allGenres.reduce((accumulator, genre) => {
          accumulator[genre] = true;
          return accumulator;
        }, {});
      } else {
        this.currentGenres = genres.reduce((accumulator, genre) => {
          accumulator[genre] = true;
          return accumulator;
        }, {});
      }
      return this.currentGenres;
    });
  }

  postGenres(genresResponse) {
    this.http.post(this.SERVER_API + 'genres', genresResponse).subscribe((res) => {
      console.log(res.json());
    });
  }

  getUser() {
    this.http.get(this.SERVER_API + 'user').subscribe((response) => {
      console.log(response.json())
    });
  }

  getSongList({bpm, speed}) {
    if (bpm) {
      return this.http.get(this.SERVER_API + `song/bpm/${bpm}/search`).map((response) => {
        return response.json();
      });
    }
    if (speed) {
      this.speed = speed;
      return this.http.get(this.SERVER_API + `song/speed/${speed}/search`).map((response) => {
        return response.json();
      });
    }
  }

}
