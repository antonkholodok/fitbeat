import {Injectable} from '@angular/core';

import {Events} from "../interfaces/Events";

const soundManager = require('soundmanager2').soundManager;

@Injectable()
export class PlayerService {

  private soundObject: any;
  private subscribers: Object = {};
  private lastSong;

  private SERVER_API = '/api/';

  initialize(song, callback: (e: Error, data: any) => void) {
    if (this.lastSong) {
      soundManager.unload(this.lastSong.id);
      soundManager.destroySound(this.lastSong.id);
    }

    const soundObject = soundManager.createSound({
      url: this.SERVER_API + 'stream/' + song.path ,
      id: song.id,
      volume: 100,
      onbufferchange: () => this.publish(Events.BufferingStart, null),
      ondataerror: () => this.publish(Events.AudioError, null),
      onfinish: () => this.publish(Events.Finish, null),
      onload: () => this.publish(Events.BufferingStart, null),
      onpause: () => this.publish(Events.Pause, null),
      onplay: () => this.publish(Events.Play, null),
      onresume: () => this.publish(Events.PlayResume, null),
      onstop: () => this.publish(Events.Finish, null),
      whileplaying: () => {
        const time = this.currentTime();
        this.publish(Events.Time, time)
      },
    });

    if (!soundObject) {
      return callback(new Error('Error while create sound'), null);
    }

    this.lastSong = song;

    soundObject.play();
    this.soundObject = soundObject;
    return callback(null, song);
  }

  play() {
    if (this.soundObject) {
      this.soundObject.resume();
    }
  }

  pause() {
    if (this.soundObject) {
      this.soundObject.pause();
    }
  }

  seek(percent: number) {
    if (this.soundObject) {
      const time = this.soundObject.duration * percent / 100;
      this.soundObject.setPosition(time);
    }
  }

  currentTime(): number {
    if (!this.soundObject) return;
    return this.soundObject.position;
  }

  totalTime():number {
    if (!this.soundObject) return;
    return this.soundObject.duration;
  }

  setVolume(value: number) {
    if (!this.soundObject) return;
    this.soundObject.setVolume(value);
  }

  getVolume(): number {
    if (!this.soundObject) return;
    return this.soundObject.volume;
  }

  on(event, handler: (data) => void) {
    if (!this.subscribers[event]) this.subscribers[event] = [];
    this.subscribers[event].push(handler);
  }

  publish(event, data) {
    if (this.subscribers[event]) {
      this.subscribers[event].forEach(function (handler) {
        handler(data);
      });
    }
  }

}
