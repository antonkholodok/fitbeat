import {Injectable} from '@angular/core';
import {PlayerService} from "./player.service";
import {Events} from "../interfaces/Events";
import {BackendService} from "./backend.service";
import {$WebSocket} from "angular2-websocket/angular2-websocket";

@Injectable()
export class SoundManagerService {

  private subscribers: Object = {};
  private currentSong: Object;
  private isPlaying = false;
  private isMute = false;

  private bpm;

  private webSocket: $WebSocket;

  constructor(private playerService: PlayerService, private backendService: BackendService) {

    this.webSocket = new $WebSocket('ws://127.0.0.1:4200/hr');

    this.webSocket.getDataStream().subscribe((data) => {
      const d = JSON.parse(data.data);
      if (d["nextSong"]) {
      }
      this.bpm = d["value"];
      this.next();
      this.backendService.currentHr.next(d["value"]);
    });

    this.subscribePlayerService(this.playerService);

    // this.backendService.currentSong.subscribe((song) => {
    //   this.play(song);
    // });

  }

  play(song: Object) {
    this.currentSong = song;

    this.playerService.initialize(song, (e: Error) => {
      if (!e) {
        // this.playerService.play();
        this.publish(Events.ChangeSong, song);
        this.isPlaying = true;
      } else {
        alert(e.message);
      }
    });
  }

  togglePlayPause() {
    if (this.currentSong != null) {
      if (!this.isPlaying) {
        this.playerService.play();
      } else {
        this.playerService.pause();
      }
    }
  }

  next() {
    this.backendService.getSong({bpm: this.bpm, speed: this.backendService.speed}).subscribe((song) => {
      this.play(song);
    });
  }

  seek(time: number) {
    if (this.playerService && this.currentSong) {
      this.playerService.seek(time);
    }
  }

  toggleMute() {
    if (this.currentSong) {
      if (this.isMute) {
        this.playerService.setVolume(100);
        this.isMute = false;
        this.publish(Events.Volume, false);
      } else {
        this.playerService.setVolume(0);
        this.isMute = true;
        this.publish(Events.Volume, true);
      }
    }
  }

  getTotalTime() {
    if (this.playerService && this.currentSong) {
      return this.playerService.totalTime();
    }
    return null;
  }

  on(event, handler: any) {
    if (!this.subscribers[event]) this.subscribers[event] = [];
    this.subscribers[event].push(handler);
  }

  private publish(event, data: any) {
    // console.log('Publish event:', event, data);
    if (this.subscribers[event]) {
      this.subscribers[event].forEach(function (handler) {
        handler(data);
      });
    }
  }

  getCurrentSong(): Object {
    return this.currentSong;
  }

  onSongFinish() {
    this.backendService.getSong().subscribe((song) => {
      this.play(song);
    });
  }

  subscribePlayerService(playerService: PlayerService) {

    playerService.on(Events.Play, () => {
      this.publish(Events.Play, null);
      this.isPlaying = true;
    });

    playerService.on(Events.PlayStart, () => {
      this.publish(Events.PlayStart, null);
      this.isPlaying = true;
    });

    playerService.on(Events.PlayResume, () => {
      this.publish(Events.PlayResume, null);
      this.isPlaying = true;
    });

    playerService.on(Events.Pause, () => {
      this.publish(Events.Pause, null);
      this.isPlaying = false;
    });

    playerService.on(Events.Finish, () => {
      this.publish(Events.Finish, null);
      this.isPlaying = false;
      this.onSongFinish();
    });

    playerService.on(Events.Seek, () => this.publish(Events.Seek, null));

    playerService.on(Events.Seeked, () => this.publish(Events.Seeked, null));

    playerService.on(Events.Time, (time) => {
      this.publish(Events.Time, time);
    });

    playerService.on(Events.AudioError, () => {
      this.publish(Events.AudioError, null);
      this.isPlaying = false;
    });

    playerService.on(Events.NoStreams, () => this.publish(Events.NoStreams, null));

  }
}
