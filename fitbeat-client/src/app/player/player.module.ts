import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PlayerComponent} from './player.component';
import {SoundManagerService} from "../services/sound-manager.service";
import {PlayerService} from "../services/player.service";
import {BackendService} from "../services/backend.service";

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [PlayerComponent],
  providers: [SoundManagerService, PlayerService, BackendService],
  exports: [PlayerComponent]
})
export class PlayerModule {
}
