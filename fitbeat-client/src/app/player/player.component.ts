import {Component, OnInit} from '@angular/core';
import {SoundManagerService} from "../services/sound-manager.service";

@Component({
  selector: 'player',
  templateUrl: './player.component.html',
  styleUrls: ['./player.component.css']
})
export class PlayerComponent implements OnInit {

  constructor(public soundManagerService: SoundManagerService) {
  }

  ngOnInit() {
  }

  togglePlayPause() {
    this.soundManagerService.togglePlayPause();
  }

  next() {
    this.soundManagerService.next();
  }


}
