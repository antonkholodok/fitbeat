import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';

import {AppComponent} from './app.component';

import {OAuthService} from 'angular2-oauth2/oauth-service';
import {RouterModule, Routes} from "@angular/router";
import {LocationStrategy, HashLocationStrategy, APP_BASE_HREF, CommonModule} from "@angular/common";
import {TagsComponent} from "./tags/tags.component";
import {RunningComponent} from "./running/running.component";
import {HeartRateComponent} from "./heart-rate/heart-rate.component";
import {BackendService} from "./services/backend.service";
import {PlayerModule} from "./player/player.module";
import {NavigationComponent} from "./navigation/navigation.component";
import { SearchComponent } from './search/search.component';


export const routes: Routes = [
  { path: '', redirectTo: 'heart-rate', pathMatch: 'full' },
  {path: 'tags', component: TagsComponent},
  {path: 'running', component: RunningComponent},
  {path: 'heart-rate', component: HeartRateComponent},
  {path: 'search', component: SearchComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    TagsComponent,
    RunningComponent,
    NavigationComponent,
    HeartRateComponent,
    SearchComponent
  ],
  imports: [
    CommonModule,
    BrowserModule,
    FormsModule,
    HttpModule,
    PlayerModule,
    RouterModule.forRoot(routes)
  ],
  providers: [
    BackendService,
    {provide: LocationStrategy, useClass: HashLocationStrategy},
    {provide: APP_BASE_HREF, useValue: '/'}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}



