import {Component, OnInit} from '@angular/core';
import {BackendService} from "../services/backend.service";
import {Observable} from "rxjs";
import {FormsModule} from "@angular/forms";
import {omitBy} from 'lodash';

@Component({
  selector: 'tags',
  templateUrl: './tags.component.html',
  styleUrls: ['./tags.component.css']
})
export class TagsComponent implements OnInit {

  constructor(public backendService: BackendService) {
  }

  ngOnInit() {
    this.backendService.getGenres().subscribe((genres) => {

    });
  }

  onChangeCheckbox(event, form) {
    const genresResponse = {genres: Object.keys(omitBy(form.value, (val) => !val))};
    this.backendService.postGenres(genresResponse);
  }

}
