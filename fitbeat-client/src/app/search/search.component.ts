import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from "@angular/router";
import {BackendService} from "../services/backend.service";
import {SoundManagerService} from "../services/sound-manager.service";
import {Events} from "../interfaces/Events";

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  public bpm;

  public speed;

  public searchValue;

  songsList = [];

  searchType = 'bpm';

  currentSong = null;

  constructor(private route: ActivatedRoute, private router: Router, private backendService: BackendService, private soundManagerService: SoundManagerService) {
  }

  ngOnInit() {
    this.route
      .queryParams.take(1).subscribe((params => {
      if (params['speed']) {
        this.searchType = 'speed';
        this.speed = params['speed'] || '9';
        this.searchValue = this.speed;
        this.backendService.lastSearchParams = {speed: this.speed};
        this.searchSong(this.speed)

      } else {
        this.searchType = 'bpm';
        this.bpm = params['bpm'] || '80';
        this.searchValue = this.bpm;
        this.backendService.lastSearchParams = {bpm: this.bpm};
        this.searchSong(this.bpm)
      }
    }));
  }

  goBack() {
    window.history.back();
  }

  searchSong(searchValue) {
    this.backendService.lastSearchParams = {};
    this.backendService.lastSearchParams[this.searchType] = searchValue;

    this.backendService.getSongList(this.backendService.lastSearchParams).subscribe((list) => {
      this.songsList = list.map((songItem) => {
        const names = songItem.name.split('-');
        songItem.name = names[1];
        songItem.author = names[0];
        return songItem;
      });
    })
  }

  playSongs(startSong) {
    this.currentSong = startSong;
    this.soundManagerService.play(startSong);
    if(this.searchType === 'bpm') {
      this.router.navigate(['heart-rate']);
    } else {
      this.router.navigate(['running']);
    }
  }

}
