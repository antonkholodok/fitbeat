import {Component, OnInit} from '@angular/core';
import {BackendService} from "../services/backend.service";
import {SoundManagerService} from "../services/sound-manager.service";
import {Router} from "@angular/router";

@Component({
  host: {class: 'data'},
  selector: 'heart-rate',
  templateUrl: './heart-rate.component.html',
  styleUrls: ['./heart-rate.component.css']
})
export class HeartRateComponent implements OnInit {

  public bpm = 80;

  constructor(private router: Router, private backendService: BackendService, private soundManagerService: SoundManagerService) {
    backendService.currentHr.subscribe((value) => {
      this.bpm = value;
    });
  }

  ngOnInit() {
  }

  searchSong(bmp) {
    this.router.navigate(['search'], {queryParams: {bpm: this.bpm}});
  }

}
