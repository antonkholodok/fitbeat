import {Component} from '@angular/core';
import {BackendService} from "./services/backend.service";
import {Router} from "@angular/router";
import {SoundManagerService} from "./services/sound-manager.service";

@Component({
  selector: 'body',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  constructor(private backendService: BackendService, public router: Router, public soundManagerService: SoundManagerService) {
    this.backendService.getUser();
  }

}
