import { FitbeatsClientPage } from './app.po';

describe('fitbeats-client App', function() {
  let page: FitbeatsClientPage;

  beforeEach(() => {
    page = new FitbeatsClientPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
